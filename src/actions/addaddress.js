import {SERVER_DISPATCH,DISPATCH_SUCCESS,DISPATCH_ERROR} from '../constants'
import {reactLocalStorage} from 'reactjs-localstorage';
import {addressList} from './addressList'
import {reset} from 'redux-form';
import axios from 'axios'
export const serverDispatch=()=> {
  return {
    type: SERVER_DISPATCH+'addaddress',
  }
}

export const dispatchSuccess=()=>{
  return {
    type: DISPATCH_SUCCESS+'addaddress'
  }
}

export const dispatchError=(msg)=> {
  return {
    type: DISPATCH_ERROR+'addaddress',
    msg:msg
  }
}

export const addAddress=(values)=>{
  return (dispatch)=>{
    dispatch(serverDispatch());
    axios({
      method: 'POST',
      url: 'http://3dmarket.app/apicabinet/index.php',
      data: {
        method:'address_add',
        token: reactLocalStorage.get('authToken'),
        values:values
      },
    }).then(function (response) {
      if(response.data.type==='ok'){
        dispatch(dispatchSuccess());
        dispatch(addressList());
        dispatch(reset('AddAddress'))
      }else{
        dispatch(dispatchError(response.data.msg));
      }
    }).catch(function (error) {
        dispatch(dispatchError(error));
    });
  }
}
