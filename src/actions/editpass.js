import {SERVER_DISPATCH,DISPATCH_SUCCESS,DISPATCH_ERROR} from '../constants'
import {reactLocalStorage} from 'reactjs-localstorage';
import axios from 'axios'
import {reset} from 'redux-form';
export const serverDispatch=()=> {
  return {
    type: SERVER_DISPATCH+'editpass',
  }
}

export const dispatchSuccess=()=>{
  return {
    type: DISPATCH_SUCCESS+'editpass',
  }
}

export const dispatchError=(msg)=> {
  return {
    type: DISPATCH_ERROR+'editpass',
    msg:msg
  }
}

export const editpass=(values)=>{
  return (dispatch)=>{
    dispatch(serverDispatch());
    axios({
      method: 'POST',
      url: 'http://3dmarket.app/apicabinet/index.php',
      data: {
        method:'user_pass_edit',
        token: reactLocalStorage.get('authToken'),
        values:values
      },
    }).then(function (response) {
      if(response.data.type==='ok'){
        dispatch(dispatchSuccess());
        dispatch(reset('Editpass'))
      }else{
        dispatch(dispatchError(response.data.msg));
      }
    }).catch(function (error) {
        dispatch(dispatchError());
    });
  }
}
