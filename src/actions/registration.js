import {SERVER_DISPATCH,DISPATCH_SUCCESS,DISPATCH_ERROR} from '../constants'
import {reactLocalStorage} from 'reactjs-localstorage';
import axios from 'axios'
export const serverDispatch=()=> {
  return {
    type: SERVER_DISPATCH+'registration',
  }
}

export const dispatchSuccess=(data)=>{
  return {
    type: DISPATCH_SUCCESS+'registration',
  }
}

export const dispatchError=(msg)=> {
  return {
    type: DISPATCH_ERROR+'registration',
    msg:msg
  }
}

export const registration=(values,history)=>{
  return (dispatch)=>{
    dispatch(serverDispatch());
    axios({
      method: 'POST',
      url: 'http://3dmarket.app/apicabinet/index.php',
      data: {
        method:'user_insert',
        values: values
      },
    }).then(function (response) {
      if(response.data.type==='ok'){
        reactLocalStorage.set('authToken',response.data.authToken);
        dispatch(dispatchSuccess());
        history.push('/cabinet/history');
      }else{
        dispatch(dispatchError(response.data.msg));
      }
    }).catch(function (error) {
        dispatch(dispatchError(error));
    });
  }
}
