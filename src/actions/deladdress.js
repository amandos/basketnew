import {SERVER_DISPATCH,DISPATCH_SUCCESS,DISPATCH_ERROR} from '../constants'
import {reactLocalStorage} from 'reactjs-localstorage';
import {addressList} from './addressList'
import axios from 'axios'
export const serverDispatch=()=> {
  return {
    type: SERVER_DISPATCH+'deladdress',
  }
}

export const dispatchSuccess=()=>{
  return {
    type: DISPATCH_SUCCESS+'deladdress',
  }
}

export const dispatchError=(msg)=> {
  return {
    type: DISPATCH_ERROR+'deladdress',
    msg:msg
  }
}

export const delAddress=(values)=>{
  return (dispatch)=>{
    dispatch(serverDispatch());
    axios({
      method: 'POST',
      url: 'http://3dmarket.app/apicabinet/index.php',
      data: {
        method:'address_delete',
        token: reactLocalStorage.get('authToken'),
        address_id:values
      },
    }).then(function (response) {
      if(response.data.type==='ok'){
        dispatch(addressList());
        dispatch(dispatchSuccess());
      }else{
        dispatch(dispatchError(response.data.msg));
      }
    }).catch(function (error) {
        dispatch(dispatchError());
    });
  }
}
