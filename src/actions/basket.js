import {SERVER_DISPATCH,DISPATCH_SUCCESS,DISPATCH_ERROR} from '../constants'
import {reactLocalStorage} from 'reactjs-localstorage';
import axios from 'axios'
var base64 = require('base-64');
export const serverDispatch=()=> {
  return {
    type: SERVER_DISPATCH+'basketlist',
  }
}
export const dispatchSuccess=(data)=>{
  return {
    type: DISPATCH_SUCCESS+'basketlist',
    data:data,
  }
}
export const dispatchError=(msg)=> {
  return {
    type: DISPATCH_ERROR+'basketlist',
    msg:msg
  }
}
export const basketList=()=>{
  return (dispatch)=>{
    var list = reactLocalStorage.get('productsBasket');
    if(list!==undefined){

        list = base64.decode(list)
        list = JSON.parse(list);
        if(list.length===0){
          dispatch(dispatchError('Ваше корзина пуста!'));
          return;
        }
    }else{
      dispatch(dispatchError('Ваше корзина пуста!'));
      return;
    }
    dispatch(serverDispatch());
    axios({
      method: 'POST',
      url: 'http://3dmarket.app/apicabinet/index.php',
      data: {
        method:'product_basket_list',
        data: list,
      },
    }).then(function (response) {
      if(response.data.type==='ok'){
        dispatch(dispatchSuccess(response.data.data));
      }else{
        dispatch(dispatchError(response.data.msg));
      }
    }).catch(function (error) {
        dispatch(dispatchError(error));
    });
  }
}
