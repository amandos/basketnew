import {SERVER_DISPATCH,DISPATCH_SUCCESS,DISPATCH_ERROR} from '../constants'
import {reactLocalStorage} from 'reactjs-localstorage';
import axios from 'axios'
export const serverDispatch=()=> {
  return {
    type: SERVER_DISPATCH+'orderInsert',
  }
}

export const dispatchSuccess=(data)=>{
  return {
    type: DISPATCH_SUCCESS+'orderInsert',
    data:data,
  }
}

export const dispatchError=(msg)=> {
  return {
    type: DISPATCH_ERROR+'orderInsert',
    msg:msg
  }
}

export const orderInsert=(values,history)=>{
  return (dispatch)=>{
    var orderId = reactLocalStorage.get('orderId');
    if(orderId>0){
      dispatch(dispatchSuccess());
      reactLocalStorage.set('orderId',orderId);
      history.push(`/basket/payment/${orderId}`);
    }else{
      dispatch(serverDispatch());
      axios({
        method: 'POST',
        url: 'http://3dmarket.app/apicabinet/index.php',
        data: {
          method:'order_insert',
          token: reactLocalStorage.get('authToken') ? reactLocalStorage.get('authToken') : '',
          values
        },
      }).then(function (response) {
        if(response.data.type==='ok'){
          if(response.data.token){
            let token = JSON.parse(response.data.token);
            reactLocalStorage.set('authToken',token.authToken);
          }
          dispatch(dispatchSuccess());
          reactLocalStorage.set('orderId',response.data.orderid);
          history.push(`/basket/payment/${response.data.orderid}`);

        }else{
          dispatch(dispatchError(response.data.msg));
        }
      }).catch(function (error) {
          dispatch(dispatchError(error));
      });
    }

  }
}
