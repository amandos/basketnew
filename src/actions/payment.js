import {SERVER_DISPATCH,DISPATCH_SUCCESS,DISPATCH_ERROR} from '../constants'
import {reactLocalStorage} from 'reactjs-localstorage';
import { step} from './step'
import axios from 'axios'
export const successFalse=()=> {
  return {
    type: 'SUCCESS_FALSE',
  }
}
export const serverDispatch=()=> {
  return {
    type: SERVER_DISPATCH+'payment',
  }
}
export const dispatchSuccess=(data)=>{
  return {
    type: DISPATCH_SUCCESS+'payment',
    data:data,
  }
}
export const dispatchError=(msg)=> {
  return {
    type: DISPATCH_ERROR+'payment',
    msg:msg
  }
}
export const payment=(id,type)=>{
  return (dispatch)=>{
    dispatch(serverDispatch());
    reactLocalStorage.set('orderId',0);
    reactLocalStorage.set('address_id',0);
    reactLocalStorage.set('addresschange',0);
    reactLocalStorage.set('apartment','');
    reactLocalStorage.set('delivery_time','');
    reactLocalStorage.set('house','');
    reactLocalStorage.set('productsBasket','');
    reactLocalStorage.set('street','');
    axios({
      method: 'POST',
      url: 'http://3dmarket.app/apicabinet/index.php',
      data: {
        method:'payment_order',
        token: reactLocalStorage.get('authToken') ? reactLocalStorage.get('authToken') : '',
        order_id: id,
        payment_type: type
      },
    }).then(function (response) {
      if(response.data.type==='ok'){

        dispatch(dispatchSuccess(response.data.data));
        dispatch(step(3))
      }else{
        dispatch(dispatchError(response.data.msg));
      }
    }).catch(function (error) {
        dispatch(dispatchError(error));
    });
  }
}
