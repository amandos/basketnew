import {reactLocalStorage} from 'reactjs-localstorage';
var _ = require('lodash');
var base64 = require('base-64');
export const createBasket=(barcode)=> {
  reactLocalStorage.set('productsBasket',JSON.stringify([]));
  let products = JSON.parse(reactLocalStorage.get('productsBasket'));
  products.push({barcode: barcode,quantity: 1})
  products = base64.encode(JSON.stringify(products,true));
  reactLocalStorage.set('productsBasket',products);
  return {
    type: 'BASKET_ADD',
    data: JSON.parse(base64.decode(reactLocalStorage.get('productsBasket'))),
  }
}
export const updateBasket =(id)=>{
  let products = JSON.parse(base64.decode(reactLocalStorage.get('productsBasket')));
  let check = _.filter(products, function(o){
     if(o.barcode ===id){
       return o;
     }
  });
  if(check.length>0){
    let list = _.filter(products, function(o){
       if(o.barcode ===id){
          o.quantity +=1;
       }
       return true;
    });
    list = base64.encode(JSON.stringify(list,true));
    reactLocalStorage.set('productsBasket',list);
  }else{
    products.push({barcode: id,quantity: 1})
    products = base64.encode(JSON.stringify(products,true));
    reactLocalStorage.set('productsBasket',products);
  }
  return {
    type: 'BASKET_ADD',
    data: JSON.parse(base64.decode(reactLocalStorage.get('productsBasket'))),
  }
}

export const basketAdd=(barcode)=>{
  return (dispatch)=>{
    let basket = reactLocalStorage.get('productsBasket')
    if(basket===undefined){
      dispatch(createBasket(barcode));
    }else{
      dispatch(updateBasket(barcode));
    }
  }
}
