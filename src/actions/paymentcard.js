import {SERVER_DISPATCH,DISPATCH_SUCCESS,DISPATCH_ERROR} from '../constants'
import {reactLocalStorage} from 'reactjs-localstorage';
import axios from 'axios'
export const serverDispatch=()=> {
  return {
    type: SERVER_DISPATCH+'paymentcard',
  }
}
export const dispatchSuccess=(data)=>{
  return {
    type: DISPATCH_SUCCESS+'paymentcard',
    data:data,
  }
}
export const dispatchError=(msg)=> {
  return {
    type: DISPATCH_ERROR+'paymentcard',
    msg:msg
  }
}
export const paymentcard=(id,type)=>{
  return (dispatch)=>{
    dispatch(serverDispatch());
    axios({
      method: 'POST',
      url: 'http://3dmarket.app/apicabinet/index.php',
      data: {
        method:'payment_order',
        token: reactLocalStorage.get('authToken') ? reactLocalStorage.get('authToken') : '',
        order_id: id,
        payment_type: 1
      },
    }).then(function (response) {
      if(response.data.type==='ok'){
        dispatch(dispatchSuccess(response.data.data));
      }else{
        dispatch(dispatchError(response.data.msg));
      }
    }).catch(function (error) {
        dispatch(dispatchError(error));
    });
  }
}
