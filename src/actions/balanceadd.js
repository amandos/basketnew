import {SERVER_DISPATCH,DISPATCH_SUCCESS,DISPATCH_ERROR} from '../constants'
import {reactLocalStorage} from 'reactjs-localstorage';
import axios from 'axios'
export const serverDispatch=()=> {
  return {
    type: SERVER_DISPATCH+'balanceadd',
  }
}
export const dispatchSuccess=(data)=>{
  return {
    type: DISPATCH_SUCCESS+'balanceadd',
    data:data,
  }
}
export const dispatchError=(msg)=> {
  return {
    type: DISPATCH_ERROR+'balanceadd',
    msg:msg
  }
}
export const balanceadd=(sum)=>{
  return (dispatch)=>{
    dispatch(serverDispatch());
    axios({
      method: 'POST',
      url: 'http://3dmarket.app/apicabinet/index.php',
      data: {
        method:'balance_add',
        token: reactLocalStorage.get('authToken') ? reactLocalStorage.get('authToken') : '',
        sum: sum
      },
    }).then(function (response) {
      if(response.data.type==='ok'){
        dispatch(dispatchSuccess(response.data.data));
      }else{
        dispatch(dispatchError(response.data.msg));
      }
    }).catch(function (error) {
        dispatch(dispatchError(error));
    });
  }
}
