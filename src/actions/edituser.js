import {SERVER_DISPATCH,DISPATCH_SUCCESS,DISPATCH_ERROR} from '../constants'
import {reactLocalStorage} from 'reactjs-localstorage';
import {InfoAction} from './userinfo'
import axios from 'axios'
export const serverDispatch=()=> {
  return {
    type: SERVER_DISPATCH+'edituser',
  }
}

export const dispatchSuccess=()=>{
  return {
    type: DISPATCH_SUCCESS+'edituser'
  }
}

export const dispatchError=(msg)=> {
  return {
    type: DISPATCH_ERROR+'edituser',
    msg:msg
  }
}

export const editUser=(values)=>{
  return (dispatch)=>{
    dispatch(serverDispatch());
    axios({
      method: 'POST',
      url: 'http://3dmarket.app/apicabinet/index.php',
      data: {
        method:'user_info_edit',
        token: reactLocalStorage.get('authToken'),
        values:values
      },
    }).then(function (response) {
      if(response.data.type==='ok'){
        dispatch(dispatchSuccess());
        dispatch(InfoAction());
      }else{
        dispatch(dispatchError(response.data.msg));
      }
    }).catch(function (error) {
        dispatch(dispatchError(error));
    });
  }
}
