import {SERVER_DISPATCH,DISPATCH_SUCCESS,DISPATCH_ERROR} from '../constants'
import {reactLocalStorage} from 'reactjs-localstorage';
import axios from 'axios'
export const serverDispatch=()=> {
  return {
    type: SERVER_DISPATCH,
  }
}

export const dispatchSuccess=()=>{
  return {
    type: DISPATCH_SUCCESS
  }
}

export const dispatchError=(msg)=> {
  return {
    type: DISPATCH_ERROR,
    msg:msg
  }
}
export const logoutEvent=()=> {
  return {
    type: 'LOGOUT_EVENT',
  }
}
export const loginAction=(values,history)=>{
  return (dispatch)=>{
    dispatch(serverDispatch());
    axios({
      method: 'POST',
      url: 'http://3dmarket.app/apicabinet/index.php',
      data: {
        method:'user_login',
        pass:values.pass,
        phone: values.phone,
      },
    }).then(function (response) {
      if(response.data.type==='ok'){
        reactLocalStorage.set('authToken',response.data.authToken);
        dispatch(dispatchSuccess());
        history.push('/cabinet/history');
      }else{
        dispatch(dispatchError(response.data.msg));
      }
    }).catch(function (error) {
        dispatch(dispatchError());
    });
  }
}

export const logout=(history)=>{
  return (dispatch)=>{
    reactLocalStorage.set('authToken','');
    dispatch(logoutEvent());
    history.push('/login');
  }
}
