export const productsStep=()=> {
  return {
    type: 'PRODUCT_STEP',
  }
}

export const userStep=()=>{
  return {
    type: 'USER_STEP',
  }
}

export const paymentStep=()=> {
  return {
    type: 'PAYMENT_STEP',
  }
}
export const successStep=()=> {
  return {
    type: 'SUCCESS_STEP',
  }
}
export const step=(value)=>{
  return (dispatch)=>{
    if(value===0){
      dispatch(productsStep());
    }else if (value===1) {
      dispatch(userStep());
    }else if (value===2){
      dispatch(paymentStep());
    }else if (value===3){
      dispatch(successStep());
    }else{
      dispatch(productsStep());
    }
  }
}
