import React from 'react';
import { Provider } from 'react-redux'
import {render} from 'react-dom';
import { Router,Route,Switch} from 'react-router-dom'
import createBrowserHistory from 'history/createBrowserHistory'
import configureStore from './store'
//main
import {ProductlistContainer} from './containers/productlist'
import {RgtnCont} from './containers/registration'
import {LoginContainer} from './containers/login'
//cabinet
import {LayoutCont} from './containers/layout'
import {HistoryCont} from './containers/history'
import {AddressesCont} from './containers/addresses'
import {MydataCont} from './containers/mydata'
import {AddbalanceCont} from './containers/balanceadd'
//basket
import {BasketCont} from './containers/basket/'
import {BtPrdtsCont} from './containers/basket/products'
import {BtUserCont} from './containers/basket/user'
import {BtPaymentCont} from './containers/basket/payment'
//css
import 'semantic-ui-css/semantic.min.css';
import './index.css';
//configureStore
const store = configureStore();
const customHistory = createBrowserHistory();

render(
  <Provider store={store}>
    <Router history={customHistory}>
      <Switch>
        <Route exact path='/' component={ProductlistContainer} />
        <Route path='/login' component={LoginContainer} />
        <Route path='/registration' component={RgtnCont} />
        <Route path='/cabinet'>
          <LayoutCont>
            <Route path='/cabinet/history' component={HistoryCont} />
            <Route path='/cabinet/mydata' component={MydataCont} />
            <Route path='/cabinet/addresses' component={AddressesCont} />
            <Route path='/cabinet/addbalance' component={AddbalanceCont} />
          </LayoutCont>
        </Route>
        <Route path='/basket'>
          <BasketCont>
            <Route state='product' path='/basket/product' component={BtPrdtsCont} />
            <Route state='user' path='/basket/user' component={BtUserCont} />
            <Route state='payment' path='/basket/payment/:orderid' component={BtPaymentCont} />
          </BasketCont>
        </Route>
      </Switch>
    </Router>
</Provider>,
document.getElementById('root')
);
