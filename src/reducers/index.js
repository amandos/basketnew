import { combineReducers} from 'redux'
import { routerReducer } from 'react-router-redux'
import { reducer as reduxFormReducer } from 'redux-form';
import productlist from './productlist'
import basket from './basket'
import orderInsert from './orderInsert'
import login from './login'
import userinfo from './userinfo'
import addresslist from './addresslist'
import orderinfo from './orderinfo'
import payment from './payment'
import step from './step'
import orderlist from './history';
import registration from './registration';
import edituser from './useredit';
import editpass from './editpass';
import addaddress from './addaddress';
import deladdress from './deladdress';
import paymentcard from './paymentcard';
import balanceadd from './balanceadd';
const reducers = combineReducers({
  router: routerReducer,
  form: reduxFormReducer,
  productlist: productlist,
  basket:basket,
  orderInsert: orderInsert,
  login:login,
  userinfo: userinfo,
  addresslist: addresslist,
  orderinfo: orderinfo,
  payment:payment,
  step: step,
  orderlist: orderlist,
  registration:registration,
  edituser:edituser,
  editpass:editpass,
  addaddress:addaddress,
  deladdress:deladdress,
  paymentcard:paymentcard,
  balanceadd:balanceadd
})

export default reducers
