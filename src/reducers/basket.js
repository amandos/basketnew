import {SERVER_DISPATCH,DISPATCH_SUCCESS,DISPATCH_ERROR} from '../constants'
const initialState = {
  count: 0,
  products:[],
  errorserver: {status: false,msg: false},
  loading: false,
}
const basket =(state = initialState, action)=> {
  switch (action.type) {
    case 'BASKET_ADD':
      return {
        ...state,
        products:action.data,
        count:action.data.length

      }
    case SERVER_DISPATCH+'basketlist':
      return {
        ...state,
        products: [],
        errorserver: {status: false,msg: false},
        loading: true,
      }

    case DISPATCH_SUCCESS+'basketlist':
      return {
        ...state,
        products: action.data,
        errorserver: {status: false,msg: false},
        loading: false,
      }

    case DISPATCH_ERROR+'basketlist':
      return {
        ...state,
        products: [],
        errorserver: {status: true,msg: action.msg},
        loading: false,
      }
    default:
      return state
  }
}
export default basket
