import {SERVER_DISPATCH,DISPATCH_SUCCESS,DISPATCH_ERROR} from '../constants'
const initialState = {
  errorserver: {status: false,msg: false},
  loading: false,
}
const orderInsert =(state = initialState, action)=> {
  switch (action.type) {
    case SERVER_DISPATCH+'orderInsert':
      return {
        ...state,
        errorserver: {status: false,msg: false},
        loading: true,
      }

    case DISPATCH_SUCCESS+'orderInsert':
      return {
        ...state,
        errorserver: {status: false,msg: false},
        loading: false,
      }

    case DISPATCH_ERROR+'orderInsert':
      return {
        ...state,
        errorserver: {status: true,msg: action.msg},
        loading: false,
      }
    default:
      return state
  }
}
export default orderInsert
