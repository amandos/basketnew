import {SERVER_DISPATCH,DISPATCH_SUCCESS,DISPATCH_ERROR} from '../constants'
const initialState = {
  errorserver: {status: false,msg: false},
  loading: false,
  success: false
}
const payment =(state = initialState, action)=> {
  switch (action.type) {
    case 'SUCCESS_FALSE':
      return {
        ...state,
        errorserver: {status: false,msg: false},
        loading: false,
        success: false
      }

    case SERVER_DISPATCH+'payment':
      return {
        ...state,
        errorserver: {status: false,msg: false},
        loading: true,
        success: false
      }

    case DISPATCH_SUCCESS+'payment':
      return {
        ...state,
        errorserver: {status: false,msg: false},
        loading: false,
        success: true
      }

    case DISPATCH_ERROR+'payment':
      return {
        ...state,
        errorserver: {status: true,msg: action.msg},
        loading: false,
        success: false
      }
    default:
      return state
  }
}
export default payment
