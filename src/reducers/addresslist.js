import {SERVER_DISPATCH,DISPATCH_SUCCESS,DISPATCH_ERROR} from '../constants'
const initialState = {
  loading: false,
  data: [],
  errorserver: {status: false,msg: false},
}
const addresslist =(state = initialState, action)=> {
  switch (action.type) {
    case SERVER_DISPATCH+'addressList':
      return {
        ...state,
        data: [],
        errorserver: {status: false,msg: false},
        loading: true,
      }

    case DISPATCH_SUCCESS+'addressList':
      return {
        ...state,
        data: action.data,
        errorserver: {status: false,msg: false},
        loading: false,
      }

    case DISPATCH_ERROR+'addressList':
      return {
        ...state,
        data: [],
        errorserver: {status: true,msg: action.msg},
        loading: false,
      }
    default:
      return state
  }
}
export default addresslist
