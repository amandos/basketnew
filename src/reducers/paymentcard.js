import {SERVER_DISPATCH,DISPATCH_SUCCESS,DISPATCH_ERROR} from '../constants'
const initialState = {
  errorserver: {status: false,msg: false},
  code: '',
  loading: false
}
const paymentcard =(state = initialState, action)=> {
  switch (action.type) {
    case SERVER_DISPATCH+'paymentcard':
      return {
        ...state,
        errorserver: {status: false,msg: false},
        loading: true,
        code: ''
      }

    case DISPATCH_SUCCESS+'paymentcard':
      return {
        ...state,
        errorserver: {status: false,msg: false},
        loading: false,
        code: action.data
      }

    case DISPATCH_ERROR+'paymentcard':
      return {
        ...state,
        errorserver: {status: true,msg: action.msg},
        loading: false,
        code: ''
      }
    default:
      return state
  }
}
export default paymentcard
