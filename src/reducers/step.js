const initialState = {
  step: 0
}
const step =(state = initialState, action)=> {
  switch (action.type) {
    case 'PRODUCT_STEP':
      return {
        ...state,
        step: 0
      }

    case 'USER_STEP':
      return {
        ...state,
        step: 1
      }

    case 'PAYMENT_STEP':
      return {
        ...state,
        step: 2
      }
    case 'SUCCESS_STEP':
      return {
        ...state,
        step: 3
      }
    default:
      return state
  }
}
export default step
