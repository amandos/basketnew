import {SERVER_DISPATCH,DISPATCH_SUCCESS,DISPATCH_ERROR} from '../constants'
const initialState = {
  loading: false,
  data: [],
  errorserver: {status: false,msg: false},
}
const productlist =(state = initialState, action)=> {
  switch (action.type) {
    case SERVER_DISPATCH+'productlist':
      return {
        ...state,
        data: [],
        errorserver: {status: false,msg: false},
        loading: true,
      }

    case DISPATCH_SUCCESS+'productlist':
      return {
        ...state,
        data: action.data,
        errorserver: {status: false,msg: false},
        loading: false,
      }

    case DISPATCH_ERROR+'productlist':
      return {
        ...state,
        data: [],
        errorserver: {status: true,msg: action.msg},
        loading: false,
      }
    default:
      return state
  }
}
export default productlist
