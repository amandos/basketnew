import {SERVER_DISPATCH,DISPATCH_SUCCESS,DISPATCH_ERROR} from '../constants'
const initialState = {
  loading: false,
  errorserver: {status: false,msg: false},
}
const edituser =(state = initialState, action)=> {
  switch (action.type) {
    case SERVER_DISPATCH+'edituser':
      return {
        ...state,
        errorserver: {status: false,msg: false},
        loading: true,
      }

    case DISPATCH_SUCCESS+'edituser':
      return {
        ...state,
        errorserver: {status: false,msg: false},
        loading: false,
      }

    case DISPATCH_ERROR+'edituser':
      return {
        ...state,
        errorserver: {status: true,msg: action.msg},
        loading: false,
      }
    default:
      return state
  }
}
export default edituser
