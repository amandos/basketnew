import {SERVER_DISPATCH,DISPATCH_SUCCESS,DISPATCH_ERROR} from '../constants'
const initialState = {
  loading: false,
  data: [],
  errorserver: {status: false,msg: false},
}
const orderlist =(state = initialState, action)=> {
  switch (action.type) {
    case SERVER_DISPATCH+'orderList':
      return {
        ...state,
        data: [],
        errorserver: {status: false,msg: false},
        loading: true,
      }
    case DISPATCH_SUCCESS+'orderList':
      return {
        ...state,
        data: action.data,
        errorserver: {status: false,msg: false},
        loading: false,
      }

    case DISPATCH_ERROR+'orderList':
      return {
        ...state,
        data: [],
        errorserver: {status: true,msg: action.msg},
        loading: false,
      }
    default:
      return state
  }
}
export default orderlist
