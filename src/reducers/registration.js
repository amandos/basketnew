import {SERVER_DISPATCH,DISPATCH_SUCCESS,DISPATCH_ERROR} from '../constants'
const initialState = {
  loading: false,
  errorserver: {status: false,msg: false},
}
const registration =(state = initialState, action)=> {
  switch (action.type) {
    case SERVER_DISPATCH+'registration':
      return {
        ...state,
        errorserver: {status: false,msg: false},
        loading: true,
      }

    case DISPATCH_SUCCESS+'registration':
      return {
        ...state,
        errorserver: {status: false,msg: false},
        loading: false,
      }

    case DISPATCH_ERROR+'registration':
      return {
        ...state,
        errorserver: {status: true,msg: action.msg},
        loading: false,
      }
    default:
      return state
  }
}
export default registration
