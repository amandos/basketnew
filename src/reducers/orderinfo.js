import {SERVER_DISPATCH,DISPATCH_SUCCESS,DISPATCH_ERROR} from '../constants'
const initialState = {
  data: [],
  errorserver: {status: false,msg: false},
  loading: false,
}
const orderInfo =(state = initialState, action)=> {
  switch (action.type) {
    case SERVER_DISPATCH+'orderinfo':
      return {
        ...state,
        data: [],
        errorserver: {status: false,msg: false},
        loading: true,
      }

    case DISPATCH_SUCCESS+'orderinfo':
      return {
        ...state,
        data: action.data,
        errorserver: {status: false,msg: false},
        loading: false,
      }

    case DISPATCH_ERROR+'orderinfo':
      return {
        ...state,
        data: [],
        errorserver: {status: true,msg: action.msg},
        loading: false,
      }
    default:
      return state
  }
}
export default orderInfo
