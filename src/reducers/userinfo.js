import {SERVER_DISPATCH,DISPATCH_SUCCESS,DISPATCH_ERROR} from '../constants'
const initialState = {
  userinfo: [],
  loading:false,
  errorserver: {status: false,msg: false},
}
const userinfo =(state = initialState, action)=> {
  switch (action.type) {
    case SERVER_DISPATCH+'userinfo':
      return {
        ...state,
        loading: true,
        errorserver: {status: false,msg: false},
      }

    case DISPATCH_SUCCESS+'userinfo':
      return {
        ...state,
        userinfo: action.userinfo,
        loading: false,
        errorserver:{status: false,msg: false},
      }

    case DISPATCH_ERROR+'userinfo':
      return {
        ...state,
        loading: false,
        userinfo: [],
        errorserver: {status: true,msg: action.msg},
      }
    default:
      return state
  }
}
export default userinfo
