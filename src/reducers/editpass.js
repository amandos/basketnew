import {SERVER_DISPATCH,DISPATCH_SUCCESS,DISPATCH_ERROR} from '../constants'
const initialState = {
  loading:false,
  errorserver: {status: false,msg: false},
}
const editpass =(state = initialState, action)=> {
  switch (action.type) {
    case SERVER_DISPATCH+'editpass':
      return {
        ...state,
        loading: true,
        errorserver: {status: false,msg: false},
      }

    case DISPATCH_SUCCESS+'editpass':
      return {
        ...state,
        loading: false,
        errorserver:{status: false,msg: false},
      }

    case DISPATCH_ERROR+'editpass':
      return {
        ...state,
        loading: false,
        errorserver: {status: true,msg: action.msg},
      }
    default:
      return state
  }
}
export default editpass
