import {SERVER_DISPATCH,DISPATCH_SUCCESS,DISPATCH_ERROR} from '../constants'
const initialState = {
  loading: false,
  errorserver: {status: false,msg: false},
}
const deladdress =(state = initialState, action)=> {
  switch (action.type) {
    case SERVER_DISPATCH+'deladdress':
      return {
        ...state,
        errorserver: {status: false,msg: false},
        loading: true,
      }

    case DISPATCH_SUCCESS+'deladdress':
      return {
        ...state,
        errorserver: {status: false,msg: false},
        loading: false,
      }

    case DISPATCH_ERROR+'deladdress':
      return {
        ...state,
        errorserver: {status: true,msg: action.msg},
        loading: false,
      }
    default:
      return state
  }
}
export default deladdress
