import {SERVER_DISPATCH,DISPATCH_SUCCESS,DISPATCH_ERROR} from '../constants'
const initialState = {
  loading:false,
  errorserver: {status: false,msg: false},
}
const login =(state = initialState, action)=> {
  switch (action.type) {
    case SERVER_DISPATCH:
      return {
        ...state,
        loading: true,
        errorserver: {status: false,msg: false},
      }

    case DISPATCH_SUCCESS:
      return {
        ...state,
        loading: false,
        errorserver:{status: false,msg: false},
      }

    case DISPATCH_ERROR:
      return {
        ...state,
        loading: false,
        errorserver: {status: true,msg: action.msg},
      }
    default:
      return state
  }
}
export default login
