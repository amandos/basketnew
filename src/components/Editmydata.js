import React,{Component} from 'react'
import { Form,Button,Input,Label,Dimmer,Loader,Message} from 'semantic-ui-react'
import { Field,reduxForm} from 'redux-form'
import normalizePhone from './normalizePhone'
class Editmydata extends Component{
  constructor(props){
    super(props)
    this.handleSubmit = this.handleSubmit.bind(this)
  }
  renderField({input,label,required,inputLabel,type,meta: { touched, error, warning }}){
    let errorItem = false;
    if((touched)&&(error)){
      errorItem = true;
    }
    return(
      <Form.Field  required={required} error={errorItem}>
        <label>{label}</label>
        <div>
          <Input label={inputLabel ? <Button color='green'>{inputLabel}</Button> : false} {...input} type={type} />
          {
            touched && (error  && <Label basic color='red' pointing>{error}</Label>)
          }
        </div>
      </Form.Field>
    )
  }
  handleSubmit(values){
    this.props.edituser({name: values.name,phone: values.phone})
  }
  render(){
    return(
      <div>
      {
        this.props.loading  ?
            <Dimmer inverted active>
              <Loader />
            </Dimmer>
          :
            <Form onSubmit={this.props.handleSubmit(this.handleSubmit)}>
                <Field name="name" type="text" required={true} component={this.renderField} label="Имя" />
                <Field name="phone" type="text" normalize={normalizePhone} required={true} inputLabel='+7' component={this.renderField} label="Номер телефона" />
                {
                  this.props.errorserver.status
                  ?
                    <Message color='red'>{this.props.errorserver.msg}</Message>
                  :
                  false
                }
                <Button disabled={this.props.pristine || this.props.submitting} color='green'>Сохранить</Button>
            </Form>
      }
      </div>
    )
  }
}
export default reduxForm({
    form: 'Editmydata',
    enableReinitialize: true,
    validate:(values)=>{
      const errors = {}
      if (!values.name) {
        errors.name = 'Поля "Имя" не может быть пустым'
      } else if (values.name.length > 35) {
        errors.name = 'Нельзя использовать слишком много символов'
      }
      if (!values.phone) {
        errors.phone = 'Номер телефона не может быть пустым'
      } else if (values.phone.length > 35) {
        errors.phone = 'Нельзя использовать слишком много символов'
      }
      return errors
    }
  })(Editmydata);
