import React,{Component} from 'react'
import { Container,Divider,Image,Grid,Menu,List,Popup,Button} from 'semantic-ui-react'
import { NavLink } from 'react-router-dom';
class Layout extends Component{
  constructor(props){
    super(props)
    this.logout = this.logout.bind(this)
  }
  componentDidMount(){
    this.props.onUserInfo()
  }
  componentWillReceiveProps(props){
      props.userinfo.phone===undefined ? this.props.history.push('/login') : console.log('success');
  }
  logout(){
    this.props.onLogout(this.props.history)
  }
  render(){
    return(
      <div>
        <div className='orangeHeader'>
          <Container>
              <Grid padded='vertically' verticalAlign='middle'>
                <Grid.Row>
                  <Grid.Column>
                    <Image src='/img/logo_mini.png' size='mini' />
                  </Grid.Column>
                  <Grid.Column width={3} style={{textAlign: 'right'}} floated='right' verticalAlign='middle'>
                    <List floated='right'>
                      <List.Item>
                        <List.Icon style={{cursor: 'pointer'}} name='user' size='big' style={{cursor: 'pointer',color: '#fff'}}  />
                        <List.Content  verticalAlign='middle'>
                          <Popup
                            wide
                            on='click'
                            trigger={<h4 style={{float: 'left',cursor: 'pointer',color: '#fff'}}>{this.props.userinfo.name}</h4>}
                            content={<Button onClick={this.logout} color='orange' content='Выйти' fluid />}
                            position='bottom right'
                          />
                        </List.Content>
                      </List.Item>
                    </List>
                  </Grid.Column>
                </Grid.Row>
              </Grid>
          </Container>
          <Divider />
        </div>
        <Container style={{padding: '15px',backgroundColor: '#fff'}}>
          <Grid columns={2} divided>
            <Grid.Column width={3}>
              <Menu vertical secondary className='gradientGrey'>
                <Menu.Item activeClassName='active' as={NavLink} to='/cabinet/history'>
                  История заказов
                </Menu.Item>
                <Menu.Item activeClassName='active' as={NavLink} to='/cabinet/mydata'>
                  Мои данные
                </Menu.Item>
                <Menu.Item activeClassName='active' as={NavLink} to='/cabinet/addresses'>
                  Список адресов
                </Menu.Item>
                <Menu.Item activeClassName='active' as={NavLink} to='/cabinet/addbalance'>
                  Пополнить баланс
                </Menu.Item>
              </Menu>
            </Grid.Column>
            <Grid.Column width={13} >
              {this.props.children}
            </Grid.Column>
          </Grid>
        </Container>
      </div>
    )
  }
}
export default Layout;
