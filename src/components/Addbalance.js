import React,{Component} from 'react'
import { Form,Button} from 'semantic-ui-react'
class Addbalance extends Component{
  constructor(props){
    super(props)
    this.state = {
      sum: '',
      success: false
    }
    this.inputChange = this.inputChange.bind(this)
    this.payment = this.payment.bind(this)
  }
  inputChange(event){
    this.setState({sum: event.target.value})
  }
  payment(){
    this.props.onBalanceAdd(this.state.sum)
    this.setState({success: true})
  }
  render(){
    return(
      <div>
        <h2>Пополнить баланс</h2>
      {  this.state.success===false ?
        <Form>
          <Form.Field>
            <label>Сумма</label>
            <input value={this.state.sum} onChange={this.inputChange}/>
          </Form.Field>

          <Button onClick={this.payment} color='green' type='submit'>Пополнить</Button>
        </Form>
        :
        false}
        {
          this.state.success ?
            <div>
              <Form method="POST" action="https://testpay.kkb.kz/jsp/process/logon.jsp">
                  <br />
                  <Form.Field>
                    <Form.Radio label='Qazqom Epay' value='0' checked={true}  />
                  </Form.Field>
                  <input type="hidden" name="Signed_Order_B64" value={this.props.codeBA}/>
                  <input type="hidden" name="Language" value="rus"/>
                  <input type="hidden" name="BackLink" value="http://192.168.1.57/cabinet/mydata"/>
                  <input type="hidden" name="PostLink" value="http://192.168.1.57/apicabinet/pay.php"/>
                  <input type="hidden" name="FailureBackLink" value="http://192.168.1.57/cabinet/addbalance"/>
                  <input type="hidden" name="template" value="default.xsl"/>
                  <Button color='green' type='submit'>Пополнить</Button>
              </Form>
            </div>
          :
            false
        }

      </div>
    )
  }
}
export default Addbalance;
