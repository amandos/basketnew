import React,{Component} from 'react'
import { Form,Button,Grid,Input,Label,Message,Dimmer,Loader} from 'semantic-ui-react'
import { Field,reduxForm} from 'redux-form'
class Editpass extends Component{
  constructor(props){
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this)
  }
  renderField({input,label,required,inputLabel,type,meta: { touched, error, warning }}){
    let errorItem = false;
    if((touched)&&(error)){
      errorItem = true;
    }
    return(
      <Form.Field  required={required} error={errorItem}>
        <label>{label}</label>
        <div>
          <Input label={inputLabel ? inputLabel : false} {...input} type={type} />
          {
            touched && (error  && <Label basic color='red' pointing>{error}</Label>)
          }
        </div>
      </Form.Field>
    )
  }
  handleSubmit(values){
    this.props.editpass(values);
  }
  render(){
    return(
      <div>
      {
        this.props.loading  ?
            <Dimmer inverted active>
              <Loader />
            </Dimmer>
          :
            <Grid.Column width={8}>
              <Form loading={this.props.loading} onSubmit={this.props.handleSubmit(this.handleSubmit)}>
                  <Field name="pass_realed" type="password" required={true} component={this.renderField} label="Текущий пароль" />
                  <Field name="newpass" type="password" required={true} component={this.renderField} label="Новый пароль" />
                  <Field name="newpass_reload" type="password" required={true} component={this.renderField} label="Повтарите" />
                  {
                    this.props.errorserver.status ? <Message color='red'>{this.props.errorserver.msg}</Message> : false
                  }
                  <Button loading={this.props.loading} disabled={this.props.pristine || this.props.submitting} color='teal'>Сохранить</Button>
              </Form>
            </Grid.Column>
      }
      </div>
    )
  }
}
export default reduxForm({
    form: 'Editpass',
    enableReinitialize: true,
    validate:(values)=>{
      const errors = {}
      if (!values.pass_realed) {
        errors.pass_realed = 'Поле "Текущий пароль" не заполнено'
      }
      if (!values.newpass) {
        errors.newpass = 'Поле "Новый пароль" не заполнено'
      } else if (values.newpass.length > 35) {
        errors.newpass = 'Нельзя использовать слишком много символов'
      }
      if (!values.newpass_reload) {
        errors.newpass_reload = 'Поле "Подтверждение пароля" не заполнено'
      }
      if (values.newpass!==values.newpass_reload) {
        errors.newpass_reload = 'Неверное подтверждение пароля'
      }
      return errors
    }
  })(Editpass);
