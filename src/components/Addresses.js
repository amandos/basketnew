import React,{Component} from 'react'
import { Container,Grid,Icon,List,Divider,Loader,Dimmer,Message,Menu} from 'semantic-ui-react'
import Addaddress from './Addaddress'
class Addresses extends Component{
  constructor(props){
    super(props)
    this.state = {
      activeItem: 0
    }
    this.handleItemClick = this.handleItemClick.bind(this)
    this.addAddress = this.addAddress.bind(this)
  }
  componentDidMount(){
    this.props.onUserAddress()
  }
  handleItemClick(value){
    this.setState({activeItem: value})
  }
  addAddress(values){
    this.props.onAddressAdd(values)
  }
  onDeleteAddress(id){
    this.props.onDelAddress(id)
  }
  render(){
    return(
      <div>
        <Container>
          <Menu pointing secondary>
            <Menu.Item name='Список адресов' active={this.state.activeItem === 0 ? true : false}  onClick={()=>this.handleItemClick(0)} />
            <Menu.Item name='Добавить адрес' active={this.state.activeItem === 1 ? true : false}  onClick={()=>this.handleItemClick(1)} />
          </Menu>
          {
            this.state.activeItem ===0 ?
              this.props.loadingAd || this.props.loadingAdsDl  ?
                <Dimmer inverted active>
                  <Loader />
                </Dimmer>
              :
                this.props.errorserverAd.status
                ?
                  <Message color='red'>{this.props.errorserverAd.msg}</Message>
                :
                  <div>
                    {
                      this.props.errorserverAdsDl.status
                      ?
                        <Message color='red'>{this.props.errorserverAdsDl.msg}</Message>
                      :
                        false
                    }
                    <Grid>
                      <Grid.Row>
                        <Grid.Column width={3}>
                          <h4>Город</h4>
                        </Grid.Column>
                        <Grid.Column width={3}>
                          <h4>Улица/микрорайон</h4>
                        </Grid.Column>
                        <Grid.Column width={3}>
                          <h4>Дом/строение</h4>
                        </Grid.Column>
                        <Grid.Column width={3}>
                          <h4>Квартира/офис</h4>
                        </Grid.Column>
                        <Grid.Column floated='right' width={2}>
                        </Grid.Column>
                      </Grid.Row>
                      <Divider />
                    </Grid>
                    {
                      this.props.addresslist.map(function(item,index){
                        return(
                          <List.Item key={index}>
                            <Grid>
                              <Grid.Row>
                                <Grid.Column width={3}>
                                  {item.city}
                                </Grid.Column>
                                <Grid.Column width={3}>
                                  {item.street}
                                </Grid.Column>
                                <Grid.Column width={3}>
                                  № {item.house}
                                </Grid.Column>
                                <Grid.Column width={3}>
                                  {item.apartment !== undefined || item.apartment !==null || item.apartment !==""  ? <span>№ {item.apartment} </span> : ''}
                                </Grid.Column>
                                <Grid.Column floated='right' style={{textAlign: 'right'}} width={2}>
                                 <Icon style={{cursor: 'pointer'}} onClick={()=>this.onDeleteAddress(item.id)} size='large' name='remove circle' />
                                </Grid.Column>
                              </Grid.Row>
                            </Grid>
                          </List.Item>
                        )
                      },this)
                    }
                  </div>
            :
              <div>
                <Addaddress loading={this.props.loadingAds} errorserver={this.props.errorserverAds} addAddress={this.addAddress}/>
              </div>
          }
        </Container>
      </div>
    )
  }
}
export default Addresses;
