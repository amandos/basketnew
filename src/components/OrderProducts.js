  import React,{Component} from 'react'
  import { Table} from 'semantic-ui-react'
  class OrderProducts extends Component{
    constructor(props){
      super(props)
      this.state = {
        total:0,
        bonus: 0
      }

    }
    componentWillMount(){
        this.totalBasket();
    }
    totalBasket(){
      var total = 0;
      this.props.products.map(function(data){
        total += parseInt(data.total_sum,10);
        return total;
      })
      var bonus = parseInt((total*1)/100,10);
      this.setState({total: total});
      this.setState({bonus: bonus});
    }
    render(){
      return(
        <Table columns={5} size='small'>
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell>Наименование </Table.HeaderCell>
              <Table.HeaderCell>Штрих-код</Table.HeaderCell>
              <Table.HeaderCell>Цена</Table.HeaderCell>
              <Table.HeaderCell>Скидка</Table.HeaderCell>
              <Table.HeaderCell>Количество</Table.HeaderCell>
              <Table.HeaderCell>Сумма</Table.HeaderCell>
            </Table.Row>
          </Table.Header>
          <Table.Body>
              {
                this.props.products.map(function(item,index){
                  return(
                    <Table.Row key={index}>
                      <Table.Cell><h4>{item.name}</h4></Table.Cell>
                      <Table.Cell>{item.barcode}</Table.Cell>
                      <Table.Cell singleLine>{item.sum} 〒</Table.Cell>
                      <Table.Cell>{item.discount>0 ? <span>{item.discount}&nbsp; %</span> : ''}</Table.Cell>
                      <Table.Cell>{item.quantity}</Table.Cell>
                      <Table.Cell singleLine>{parseInt(item.total_sum,10)} 〒</Table.Cell>
                    </Table.Row>
                  )
                })
              }
          </Table.Body>
          <Table.Footer>
            <Table.Row>
              <Table.HeaderCell />
              <Table.HeaderCell />
              <Table.HeaderCell />
              <Table.HeaderCell><h3>Бонус: <span style={{color: '#58a000'}}>+ {this.state.bonus}</span> </h3></Table.HeaderCell>
              <Table.HeaderCell><h3>Итого:</h3></Table.HeaderCell>
              <Table.HeaderCell singleLine><h3>{this.state.total}  &nbsp; 〒</h3></Table.HeaderCell>
            </Table.Row>
          </Table.Footer>
        </Table>
      )
    }
  }

  export default OrderProducts
