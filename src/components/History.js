import React,{Component} from 'react'
import { Container,Grid,Accordion,List,Icon,Table,Divider,Button,Loader,Dimmer,Message} from 'semantic-ui-react'
class History extends Component{
  constructor(props){
    super(props)
    this.state = {
      activeIndex:'',
      orderinfo: []
    }
    this.handleClick = this.handleClick.bind(this)
    this.orderinfo =this.orderinfo.bind(this)
  }
  componentDidMount(){
    this.props.OnOrderlist()
  }
  handleClick = (e, titleProps) => {
    const { index } = titleProps
    const { orderid } = titleProps
    const { activeIndex } = this.state
    if(activeIndex !== index){
      this.orderinfo(orderid);
    }

    const newIndex = activeIndex === index ? -1 : index
    this.setState({ activeIndex: newIndex })

  }
  orderinfo(order_id){
    this.props.OnOrderinfo(order_id);
  }
  render(){
    const { activeIndex } = this.state
    return(
      <div>
        <br />
        <Container>
          {
            this.props.loadingOrderlist ?
              <Dimmer inverted active>
                <Loader />
              </Dimmer>
            :
            this.props.errorOrderlist.status
            ?
                this.props.errorOrderlist.msg ==='Пустой'
                ?
                  <h4 style={{textAlign: 'center'}}>У вас еще нет заказов</h4>
                :
                  <Message color='red'>{this.props.errorOrderlist.msg}</Message>
            :
                <div>
                  <Grid>
                    <Grid.Row>
                      <Grid.Column width={3}>
                        Дата
                      </Grid.Column>
                      <Grid.Column width={3}>
                        Время доставки
                      </Grid.Column>
                      <Grid.Column width={3}>
                        Номер заказа
                      </Grid.Column>
                      <Grid.Column width={3}>
                        Статус
                      </Grid.Column>
                      <Grid.Column floated='right' width={2}>
                      </Grid.Column>
                    </Grid.Row>
                  </Grid>
                  <Divider />
                  <List divided relaxed>
                  {
                    this.props.orderlist.length > 0 ?
                      this.props.orderlist.map(function(item,index){
                        return(
                          <Accordion key={index}>
                           <Accordion.Title active={activeIndex === index} index={index} orderid={item.id} onClick={this.handleClick}>
                             <List.Item>
                               <Grid>
                                 <Grid.Row>
                                   <Grid.Column width={3}>
                                     <h3>{item.date}</h3>
                                   </Grid.Column>
                                   <Grid.Column width={3}>
                                     <h3>{item.delivery_time}</h3>
                                   </Grid.Column>
                                   <Grid.Column width={3}>
                                     <h3 style={{color: 'green'}}>№ {item.id}</h3>
                                   </Grid.Column>
                                   <Grid.Column width={3}>
                                     <b style={{color: item.status_id ==='1' ?'#ffa900' : 'rgb(77, 119, 6)'}}>{item.status}</b>
                                   </Grid.Column>
                                   <Grid.Column floated='right' style={{textAlign: 'right'}} width={3}>
                                    <h4>{activeIndex === index ? 'Свернуть' : 'Раскрыть'}  <Icon size='huge' name='dropdown' /></h4>
                                   </Grid.Column>
                                 </Grid.Row>
                               </Grid>
                             </List.Item>
                           </Accordion.Title>
                           <Accordion.Content active={activeIndex === index}>
                             <Grid centered={true}>
                               <Grid.Row>
                                 <Grid.Column width={15}>
                                   <Table>
                                     <Table.Header>
                                       <Table.Row>
                                         <Table.HeaderCell>Название товара</Table.HeaderCell>
                                         <Table.HeaderCell>Цена</Table.HeaderCell>
                                         <Table.HeaderCell>Количество</Table.HeaderCell>
                                         <Table.HeaderCell>Скидка</Table.HeaderCell>
                                         <Table.HeaderCell>Стоимость</Table.HeaderCell>
                                       </Table.Row>
                                     </Table.Header>
                                     <Table.Body>
                                     {
                                       this.props.orderInfo.products ?
                                       this.props.orderInfo.products.map(function(product){
                                         return(
                                           <Table.Row key={product.id}>
                                             <Table.Cell>{product.name}</Table.Cell>
                                             <Table.Cell>{product.sum} 〒</Table.Cell>
                                             <Table.Cell>{product.quantity}</Table.Cell>
                                             <Table.Cell>{product.discount} %</Table.Cell>
                                             <Table.Cell>{parseInt(product.total_sum,10)} 〒</Table.Cell>
                                           </Table.Row>
                                         )
                                       })
                                       : false
                                     }

                                     </Table.Body>
                                     <Table.Footer fullWidth>
                                       <Table.Row>
                                         <Table.HeaderCell colSpan='6'>
                                           <Button onClick={()=>this.props.OnOrderReload(this.props.orderInfo.products,this.props.history)} color='orange' size='small'>
                                             Повторить заказ
                                           </Button>
                                         </Table.HeaderCell>
                                       </Table.Row>
                                     </Table.Footer>
                                   </Table>
                                 </Grid.Column>
                               </Grid.Row>
                             </Grid>
                           </Accordion.Content>
                           <Divider />
                          </Accordion>
                        )
                      },this)
                      : false
                  }
                  </List>
                </div>
          }
        </Container>
    </div>
    )
  }
}
export default History;
