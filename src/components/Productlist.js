import React,{Component} from 'react'
import { List,Button,Image,Grid,Divider} from 'semantic-ui-react'
class Productlist extends Component{
  constructor(props){
    super(props);
    this.basketAdd = this.basketAdd.bind(this)
  }
  componentDidMount(){
    this.props.onProductlist()
  }
  basketAdd(event){
    this.props.onBasketadd(event.target.id);
  }
  render(){
    return(
      <div>
        <Grid verticalAlign='middle'>
          <Grid.Row>
            <Grid.Column width={6}>
              Найменование
            </Grid.Column>
            <Grid.Column width={2}>
              Штрих-код
            </Grid.Column>
            <Grid.Column width={1}>
              Цена
            </Grid.Column>
            <Grid.Column width={1}>
              Скидка
            </Grid.Column>
          </Grid.Row>
        </Grid>
        <Divider />
        <List divided relaxed>
          {
            this.props.products ? this.props.products.map(function(item,index){
              return(
                <List.Item key={index}>
                  <List.Content>
                    <Grid verticalAlign='middle'>
                      <Grid.Row>
                        <Grid.Column width={3}>
                          <Image onError={(e)=>{e.target.src='/img/none.png'}} src={`/img/${item.barcode}.png`} size='tiny'/>
                        </Grid.Column>
                        <Grid.Column width={3}>
                          {item.name}
                        </Grid.Column>
                        <Grid.Column width={2}>
                          {item.barcode}
                        </Grid.Column>
                        <Grid.Column width={1}>
                          {item.sum}
                        </Grid.Column>
                        <Grid.Column width={1}>
                          {item.discount} %
                        </Grid.Column>
                        <Grid.Column width={4} floated='right' textAlign='right'>
                          <Button id={item.barcode} onClick={this.basketAdd}>Купить</Button>
                        </Grid.Column>
                      </Grid.Row>
                    </Grid>
                  </List.Content>
                </List.Item>
              )
            },this)
            : false
          }
        </List>
      </div>
    )
  }
}
export default Productlist;
