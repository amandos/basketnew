import React,{Component} from 'react'
import { Container,Table,Menu} from 'semantic-ui-react'
import Editmydata from './Editmydata'
import Editpass from './Editpass'
class Mydata extends Component{
  constructor(props){
    super(props)
    this.state = {
      activeItem: 0
    }
    this.handleItemClick = this.handleItemClick.bind(this)
    this.edituserSubmit = this.edituserSubmit.bind(this)
    this.editPass = this.editPass.bind(this)
  }
  handleItemClick(value){
    this.setState({activeItem: value})
  }
  edituserSubmit(values){
    this.props.onEditMydata(values)
  }
  editPass(values){
    this.props.onEditPass(values)
  }
  render(){
    return(
      <div>
        <Container>
          <Menu pointing secondary>
            <Menu.Item name='Данные' active={this.state.activeItem === 0 ? true : false} onClick={()=>this.handleItemClick(0)} />
            <Menu.Item name='Изменить данные' active={this.state.activeItem === 1 ? true : false}  onClick={()=>this.handleItemClick(1)} />
            <Menu.Item name='Изменить пароль' active={this.state.activeItem === 2 ? true : false} onClick={()=>this.handleItemClick(2)} />
          </Menu>
          {
            this.state.activeItem === 0 ?
              <Table basic='very' celled={false} >
                <Table.Body>
                  <Table.Row>
                    <Table.Cell>
                        <h4 style={{color: '#a8a8a8'}}>Пользователь</h4>
                    </Table.Cell>
                    <Table.Cell>
                        <h4>{this.props.userinfo.name}</h4>
                    </Table.Cell>
                  </Table.Row>
                  <Table.Row>
                    <Table.Cell>
                        <h4 style={{color: '#a8a8a8'}}>Номер телефона</h4>
                    </Table.Cell>
                    <Table.Cell>
                        <h4>{this.props.userinfo.phone}</h4>
                    </Table.Cell>
                  </Table.Row>
                  <Table.Row>
                    <Table.Cell>
                        <h4 style={{color: '#a8a8a8'}}>Баланс:</h4>
                    </Table.Cell>
                    <Table.Cell>
                        <h4>{this.props.userinfo.balance} 〒</h4>
                    </Table.Cell>
                  </Table.Row>
                  <Table.Row>
                    <Table.Cell>
                        <h4 style={{color: '#a8a8a8'}}>Бонусы:</h4>
                    </Table.Cell>
                    <Table.Cell>
                        <h4 style={{color: '#58a000'}}>+ {this.props.userinfo.bonus}</h4>
                    </Table.Cell>
                  </Table.Row>
                </Table.Body>
              </Table>
            :
              this.state.activeItem ===1 ?
                <Editmydata loading={this.props.loadingEd} errorserver={this.props.errorserverEd} initialValues={this.props.userinfo ? this.props.userinfo : false} edituser={this.edituserSubmit}/>
            :
              <Editpass loading={this.props.loadingEp} errorserver={this.props.errorserverEp} editpass={this.editPass}/>
          }

        </Container>
      </div>
    )
  }
}
export default Mydata;
