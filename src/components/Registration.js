import React,{Component} from 'react'
import { Button, Form, Grid,Input,Label,Image, Header,Message, Segment,Dimmer,Loader } from 'semantic-ui-react'
import { Field,reduxForm} from 'redux-form'
import normalizePhone from './normalizePhone'
class Registration extends Component{
  constructor(props){
    super(props)
    this.handleSubmit = this.handleSubmit.bind(this)
  }
  renderField({input,label,required,inputLabel,type,meta: { touched, error, warning }}){
    let errorItem = false;
    if((touched)&&(error)){
      errorItem = true;
    }
    return(
      <Form.Field  required={required} error={errorItem}>
        <label>{label}</label>
        <div>
          <Input label={inputLabel ? <Button color='green'>{inputLabel}</Button> : false} {...input} type={type} />
          {
            touched && (error  && <Label basic color='red' pointing>{error}</Label>)
          }
        </div>
      </Form.Field>
    )
  }
  handleSubmit(values){
    this.props.onRegistration(values,this.props.history)
  }
  render(){
    return(
      <div className='login-form'>
       <style>
          {`
            body > div,
            body > div > div,
            body > div > div > div.login-form {
              height: 100%;
            }
          `}
       </style>
        <Dimmer inverted active={this.props.loadingRg} >
          <Loader />
        </Dimmer>
        <Grid centered={true} verticalAlign='middle' style={{height: '100%'}}>
          <Grid.Column style={{ maxWidth: 450 }}  >
            <Header as='h2' color='green' textAlign='left'>
               <Image src='/img/logo.png' />
               Регистрация
            </Header>
            <Form onSubmit={this.props.handleSubmit(this.handleSubmit)} size='large'>
              <Segment stacked>
                 <Field name="name" type="text" required={true} component={this.renderField} label="Имя" />
                 <Field name="phone" type="text" normalize={normalizePhone} required={true} inputLabel='+7' component={this.renderField} label="Номер телефона" />
                 <Field name="pass" type="password" required={true} component={this.renderField} label="Пароль" />
                 <Field name="passreload" type="password" required={true} component={this.renderField} label="Повторите пароль" />
                 {
                   this.props.errorRg.status ?
                     <Message color='red'>{this.props.errorRg.msg}</Message>
                   :
                     false
                 }
                 <Button  disabled={this.props.pristine || this.props.submitting} color='green' fluid size='large'>Регистрация</Button>
              </Segment>
            </Form>
          </Grid.Column>
        </Grid>
      </div>
    )
  }
}
export default reduxForm({
  form: 'Registration',
  enableReinitialize: true,
  validate:(values)=>{
    const errors = {}
    if (!values.name) {
      errors.name = 'Укажите Ваше Имя'
    } else if (values.name.length > 35) {
      errors.name = 'Нельзя использовать слишком много символов'
    }
    if (!values.phone) {
      errors.phone = 'Поле "Телефон" не заполнено'
    }
    if (!values.pass) {
      errors.pass = 'Поле "Пароль" не заполнено'
    }else if (values.pass.length > 35) {
      errors.pass = 'Нельзя использовать слишком много символов'
    }else if (values.pass.length < 6) {
      errors.pass = 'Минимальное количество символов в пароле: 6. Пожалуйста, введите другой пароль.'
    }
    if (!values.passreload) {
      errors.passreload = 'Поле "Повторите пароль" не заполнено'
    }else if (values.passreload.length > 35) {
      errors.passreload = 'Нельзя использовать слишком много символов'
    }
    if (values.pass !== values.passreload) {
      errors.passreload = 'Неверное подтверждение пароля'
    }
    return errors
  }
})(Registration);
