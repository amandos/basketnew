import React,{Component} from 'react'
import { Form,Button,Grid,Input,Label,Message,Dimmer,Loader,Popup} from 'semantic-ui-react'
import { Field,reduxForm} from 'redux-form'
class Addaddress extends Component{
  constructor(props){
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this)
  }
  renderFieldAddress({input,label,required,inputLabel,type,meta: { touched, error, warning }}){
    let errorItem = false;
    if((touched)&&(error)){
      errorItem = true;
    }
    return(
      <Form.Field  required={required} error={errorItem}>
        <div>
          <Input placeholder={label} label={inputLabel ? <Button color='teal'>{inputLabel}</Button> : false} {...input} type={type} />
          {
            touched && (error  && <Label basic color='red' pointing>{error}</Label>)
          }
        </div>
      </Form.Field>
    )
  }
  handleSubmit(values){
    values.city = 'Алматы';
    this.props.addAddress(values);
  }
  render(){
    return(
      <div>
      {
        this.props.loading  ?
            <Dimmer inverted active>
              <Loader />
            </Dimmer>
          :
            <Grid.Column width={8}>
              <Form onSubmit={this.props.handleSubmit(this.handleSubmit)}>
                <Form.Field>
                  <Form.Input readOnly action={(<Popup
                    trigger={<Button color='green' icon='info circle' />}
                    content='Доставка осуществляется в пределах города '
                    hideOnScroll />)} placeholder='Алматы'
                  />
                </Form.Field>
                <Form.Field>
                  <Field name="street" type="text" required={true} component={this.renderFieldAddress} label="Улица/микрорайон" />
                </Form.Field>
                <Form.Field>
                  <Field name="house" type="text" required={true} component={this.renderFieldAddress} label="Дом/строение" />
                </Form.Field>
                <Form.Field>
                  <Field name="apartment" type="text" required={true} component={this.renderFieldAddress} label="Квартира/офис" />
                </Form.Field>
                {
                  this.props.errorserver.status ? <Message color='red'>{this.props.errorserver.msg}</Message> : false
                }
                <Button loading={this.props.loading} disabled={this.props.pristine || this.props.submitting} color='green'>Добавить</Button>
              </Form>
            </Grid.Column>
      }
      </div>
    )
  }
}
export default reduxForm({
    form: 'AddAddress',
    enableReinitialize: true,
    validate:(values)=>{
      const errors = {}
      if (!values.street) {
        errors.street = 'Пожалуйста, заполните поле Улица/микрорайон'
      }
      if (!values.house) {
        errors.house = 'Пожалуйста, заполните поле Дом/строение'
      }
      return errors
    }
  })(Addaddress);
