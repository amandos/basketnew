import React,{Component} from 'react'
import { Button, Form, Grid,Input,Label,Image,Header, Message, Segment } from 'semantic-ui-react'
import { Field,reduxForm} from 'redux-form'
import normalizePhone from './normalizePhone'
class Login extends Component{
  constructor(props){
    super(props)
    this.handleSubmit = this.handleSubmit.bind(this)
  }
  renderField({input,label,required,inputLabel,type,meta: { touched, error, warning }}){
    let errorItem = false;
    if((touched)&&(error)){
      errorItem = true;
    }
    return(
      <Form.Field  required={required} error={errorItem}>
        <label>{label}</label>
        <div>
          <Input label={inputLabel ? <Button color='green'>{inputLabel}</Button> : false} {...input} type={type} />
          {
            touched && (error  && <Label basic color='red' pointing>{error}</Label>)
          }
        </div>
      </Form.Field>
    )
  }
  handleSubmit(values){
    this.props.OnLogin(values,this.props.history)
  }
  render(){
    return(
      <div className='login-form'>
       <style>
          {`
            body > div,
            body > div > div,
            body > div > div > div.login-form {
              height: 100%;
            }
          `}
       </style>
       <Grid  centered={true} verticalAlign='middle' style={{height: '100%'}}>
         <Grid.Column style={{ maxWidth: 450 }} >
           <Header as='h2' color='green' textAlign='left'>
              <Image src='/img/logo.png' />
              Закупись с удовольствием
           </Header>
           <Form loading={this.props.loading} onSubmit={this.props.handleSubmit(this.handleSubmit)} size='large'>
             <Segment stacked>
                <Field name="phone" type="text" normalize={normalizePhone} required={true} inputLabel='+7' component={this.renderField} label="Номер телефона" />
                <Field name="pass" type="password" component={this.renderField} label="Пароль" />
                {
                  this.props.errorserver.status ?
                    <Message color='red'>{this.props.errorserver.msg}</Message>
                  :
                    false
                }
                <Button  color='green' fluid size='large'>Вход</Button>
             </Segment>
           </Form>
           <Message>
             Вы еще не зарегистрированы? <a href='/registration'>Регистрация</a>
           </Message>
         </Grid.Column>
       </Grid>
      </div>
    )
  }
}
export default reduxForm({
    form: 'login',
    enableReinitialize: true,
    validate:(values)=>{
      const errors = {}
      if (!values.phone) {
        errors.phone = 'Поле "Номер телефона" не заполнено'
      }
      if (!values.pass) {
        errors.pass = 'Поле "Пароль" не заполнено'
      }
      return errors
    }
  })(Login);
