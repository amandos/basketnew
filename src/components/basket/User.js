import React,{Component} from 'react'
import { Container,Form,Grid,Popup,Button,Label,Input,Message,Table,Dimmer,Loader,Divider} from 'semantic-ui-react'
import { Field,reduxForm} from 'redux-form'
import { SubmissionError } from 'redux-form'
import DatePicker from 'react-datepicker';
import moment from 'moment';
import normalizePhone from '../normalizePhone'
import AddressList from './AddressList'
import 'react-datepicker/dist/react-datepicker.css';
import {reactLocalStorage} from 'reactjs-localstorage';
var _ = require('lodash');
class User extends Component{
  constructor(props){
    super(props)
    this.handleSubmit = this.handleSubmit.bind(this)
    this.checkErrorForm = this.checkErrorForm.bind(this)
    this.changeAddressId = this.changeAddressId.bind(this)
    this.dateDelivery = this.dateDelivery.bind(this)
    this.state ={
      addresschange: reactLocalStorage.get('addresschange') ? parseInt(reactLocalStorage.get('addresschange'),10) : 0,
      address_id:0,
      delivery_date: reactLocalStorage.get('delivery_time') ? moment(reactLocalStorage.get('delivery_time')): moment().add(3, 'hours'),
    }
  }
  componentDidMount(){
    this.props.onStep()
    this.props.onUserCheck()
    moment().format('LTS');
  }
  dateDelivery(value){
    this.setState({delivery_date: value})

  }
  changeAddressId(id){
    this.setState({address_id: id})
  }
  renderField({input,label,required,inputLabel,type,meta: { touched, error, warning }}){
    let errorItem = false;
    if((touched)&&(error)){
      errorItem = true;
    }
    return(
      <Form.Field  required={required} error={errorItem}>
        <label>{label}</label>
        <div>
          <Input label={inputLabel ? <Button color='green'>{inputLabel}</Button> : false} {...input} type={type} />
          {
            touched && (error  && <Label basic color='red' pointing>{error}</Label>)
          }
        </div>
      </Form.Field>
    )
  }
  renderFieldAddress({input,label,required,inputLabel,type,meta: { touched, error, warning }}){
    let errorItem = false;
    if((touched)&&(error)){
      errorItem = true;
    }
    return(
      <Form.Field  required={required} error={errorItem}>
        <div>
          <Input placeholder={label} label={inputLabel ? <Button color='green'>{inputLabel}</Button> : false} {...input} type={type} />
          {
            touched && (error  && <Label basic color='red' pointing>{error}</Label>)
          }
        </div>
      </Form.Field>
    )
  }
  checkErrorForm(values){
    var errors = {};
    if(!this.props.userinfo.phone){
      if (!values.name) {
        errors.name = 'Поле "Имя" не заполнено'
      } else if (values.name.length > 35) {
        errors.name = 'Нельзя использовать слишком много символов'
      }
      if (!values.phone) {
        errors.phone = 'Поле "Телефон" не заполнено'
      }
      if (!values.pass) {
        errors.pass = 'Поле "Пароль" не заполнено'
      }else if (values.pass.length > 35) {
        errors.pass = 'Нельзя использовать слишком много символов'
      }
      if (!values.passreload) {
        errors.passreload = 'Поле "Повторите пароль" не заполнено'
      }else if (values.passreload.length > 35) {
        errors.passreload = 'Нельзя использовать слишком много символов'
      }
      if (values.pass !== values.passreload) {
        errors.passreload = 'Неверное подтверждение пароля'
      }
    }
    if((!this.props.userinfo.phone) && this.state.addresschange===0){
      if (!values.street) {
        errors.street = 'Поле "Улица/микрорайон" не заполнено'
      }
      if (!values.house) {
        errors.house = 'Поле "Дом/строение" не заполнено'
      }
    }
    if((this.props.userinfo.phone) && this.state.addresschange===0){
      if (!values.street) {
        errors.street = 'Поле "Улица/микрорайон" не заполнено'
      }
      if (!values.house) {
        errors.house = 'Поле "Дом/строение" не заполнено'
      }
    }
    if(this.state.addresschange===1){
      if(this.state.address_id === 0){
        errors.address = 'Пожалуйста, выберите адресс'
      }
    }

    if(_.isEmpty(errors)){
      return true;
    }else{
      throw new SubmissionError(errors);
    }

  }
  handleSubmit(values){
    let checkError = this.checkErrorForm(values);

    if(checkError){
      values.city='Алматы';
      values.products_list = this.props.onBasketProducts();
      if(this.state.addresschange===1){
        values.address_id = this.state.address_id;
        reactLocalStorage.set('addresschange',this.state.addresschange);
        reactLocalStorage.set('address_id',this.state.address_id);
      }else{
        reactLocalStorage.set('addresschange',this.state.addresschange);
        reactLocalStorage.set('street',values.street);
        reactLocalStorage.set('house',values.house);
        reactLocalStorage.set('apartment',values.apartment ? values.apartment : '');
      }
      if(!values.apartment){
        values.apartment = ''
      }
      reactLocalStorage.set('delivery_time',values.street);
      values.delivery_time = this.state.delivery_date._d;
      reactLocalStorage.set('delivery_time',values.delivery_time);
      this.props.onOrderInsert(values,this.props.history);
    }
  }
  addressChange(value){
    this.setState({addresschange: value})
  }
  render(){
    return(
        <Container style={{padding: '15px',backgroundColor: '#fff'}}>
            <Dimmer inverted active={this.props.loading} >
              <Loader />
            </Dimmer>
            <Form onSubmit={this.props.handleSubmit(this.handleSubmit)}>
              {
                this.props.userinfo.phone ?
                <div>
                  <h2>Контактные данные</h2>
                  <Table basic='very' celled={false} >
                    <Table.Body>
                      <Table.Row>
                        <Table.Cell>
                          <h4 style={{color: '#a8a8a8'}}>Пользователь</h4>
                        </Table.Cell>
                        <Table.Cell>
                          <h4>{this.props.userinfo.name}</h4>
                        </Table.Cell>
                        </Table.Row>
                        <Table.Row>
                        <Table.Cell>
                          <h4 style={{color: '#a8a8a8'}}>Номер телефона</h4>
                        </Table.Cell>
                        <Table.Cell>
                          <h4>{this.props.userinfo.phone}</h4>
                        </Table.Cell>
                      </Table.Row>
                    </Table.Body>
                  </Table>
                </div>
                :
                <div>
                  <h2>Регистрация</h2>
                  <Form.Field>
                    <Field name="name" type="text" required={true}  component={this.renderField} label="Имя" />
                  </Form.Field>
                  <Form.Field>
                    <Field name="phone" type="text" normalize={normalizePhone} required={true} inputLabel='+7' component={this.renderField} label="Номер телефона" />
                  </Form.Field>
                  <Form.Field>
                    <Field name="pass" type="password" required={true} component={this.renderField} label="Пароль" />
                  </Form.Field>
                  <Form.Field>
                    <Field name="passreload" type="password" required={true} component={this.renderField} label="Повторить пароль" />
                  </Form.Field>
                </div>
              }
              <div>
                <br/>
                <h5>Доставка - БЕСПЛАТНО</h5>
                {
                this.props.userinfo.phone ?
                  <div>
                    <Button type='button' color={this.state.addresschange ===0 ? 'orange' : 'grey'} onClick={()=>this.addressChange(0)} basic size='big'>Добавить новую</Button>
                    <Button type='button' color={this.state.addresschange ===1 ? 'orange' : 'grey'} onClick={()=>this.addressChange(1)} basic size='big'>Выбрать из списка</Button>
                {
                  this.state.addresschange===0 ?
                  <div>
                    <br />
                    <h4>Доставка осуществляется в рамках границ, отмеченных на карте.</h4>
                    <Grid>
                      <Grid.Row>
                        <Grid.Column width={8}>
                          <iframe src="https://www.google.com/maps/d/embed?mid=1rTG9-bryceMPseRnkp2rRD9ZUsXk7A9c" width="100%" height="480"></iframe>
                        </Grid.Column>
                        <Grid.Column width={8}>
                          <Form.Field >
                            <Form.Input readOnly action={(<Popup
                              trigger={<Button color='green' icon='info circle' />}
                              content='Укажите ваш адрес согласна отмеченный на карте территории.'
                              hideOnScroll />)} placeholder='Алматы'
                            />
                          </Form.Field>
                          <Form.Field>
                            <Field value={reactLocalStorage.get('street') ? reactLocalStorage.get('street') : ''} name="street" type="text" required={true} component={this.renderFieldAddress} label="Улица" />
                          </Form.Field>
                          <Form.Field>
                            <Field name="house" type="text" required={true} component={this.renderFieldAddress} label="Дом/строение" />
                          </Form.Field>
                          <Form.Field>
                            <Field name="apartment" type="text" required={true} component={this.renderFieldAddress} label="Квартира/офис" />
                          </Form.Field>
                          <br />
                          <div>
                            <h5>Ближайшее время, когда мы можем доставить товар:</h5>
                            <DatePicker
                                showTimeSelect
                                minTime={moment().add(3, 'hours')}
                                maxTime={moment().hours(23).minutes(0)}
                                selected={this.state.delivery_date}
                                onChange={this.dateDelivery}
                                minDate={moment()}
                                maxDate={moment().add(2,"days")}
                                locale="ru"
                                dateFormat="LLL"
                                timeFormat="HH:mm"
                                shouldCloseOnSelect={false}
                            />
                          </div>
                          {
                            this.props.errorserver.status ?
                              <Message color='red'>{this.props.errorserver.msg}</Message>
                            :
                              false
                          }
                          <Divider />
                          <Button floated='right' color='orange'>Оформить заказ</Button>
                          <br style={{lineHeight: 4}}/>
                        </Grid.Column>
                      </Grid.Row>
                    </Grid>
                  </div>
                  :
                  <div>
                    <Field required={true} changeAddress={this.changeAddressId} name="address" addresslist={this.props.onUserAddress} data={this.props.addresslist} component={AddressList}/>
                    <br />
                    <div>
                      <h5>Ближайшее время, когда мы можем доставить товар:</h5>
                      <DatePicker
                          showTimeSelect
                          minTime={moment().add(3, 'hours')}
                          maxTime={moment().hours(23).minutes(0)}
                          selected={this.state.delivery_date}
                          onChange={this.dateDelivery}
                          minDate={moment()}
                          maxDate={moment().add(2,"days")}
                          locale="ru"
                          dateFormat="LLL"
                          timeFormat="HH:mm"
                          shouldCloseOnSelect={false}
                      />
                    </div>
                    <Divider />
                    <Button floated='right' color='orange'>Оформить заказ</Button>
                    <br style={{lineHeight: 4}}/>
                  </div>
                }
              </div>
              :
              <div>
                <br />
                <h4>Доставка осуществляется в рамках границ, отмеченных на карте.</h4>
                <Grid>
                  <Grid.Row>
                    <Grid.Column width={8}>
                    <iframe src="https://www.google.com/maps/d/embed?mid=1rTG9-bryceMPseRnkp2rRD9ZUsXk7A9c" width="100%" height="480"></iframe>
                    </Grid.Column>
                    <Grid.Column width={8}>
                      <Form.Field>
                        <Form.Input readOnly action={(<Popup
                          trigger={<Button color='green' icon='info circle' />}
                          content='Укажите ваш адрес согласна отмеченный на карте территории.'
                          hideOnScroll />)} placeholder='Алматы'
                        />
                      </Form.Field>
                      <Form.Field>
                        <Field name="street" type="text" required={true} component={this.renderFieldAddress} label="Улица/микрорайон" />
                      </Form.Field>
                      <Form.Field>
                        <Field name="house" type="text" required={true} component={this.renderFieldAddress} label="Дом/строение" />
                      </Form.Field>
                      <Form.Field>
                        <Field name="apartment" type="text" required={true} component={this.renderFieldAddress} label="Квартира/офис" />
                      </Form.Field>
                      <br />
                      <div>
                        <h5>Ближайшее время, когда мы можем доставить товар:</h5>
                        <DatePicker
                            showTimeSelect
                            minTime={moment().add(3, 'hours')}
                            maxTime={moment().hours(23).minutes(0)}
                            selected={this.state.delivery_date}
                            onChange={this.dateDelivery}
                            minDate={moment()}
                            maxDate={moment().add(2,"days")}
                            locale="ru"
                            dateFormat="LLL"
                            timeFormat="HH:mm"
                            shouldCloseOnSelect={false}
                        />
                      </div>
                      {
                      this.props.errorserver.status ?
                        <Message color='red'>{this.props.errorserver.msg}</Message>
                      :
                        false
                      }
                      <Divider />
                      <Button floated='right' color='orange'>Оформить заказ</Button>
                      <br style={{lineHeight: 4}}/>
                    </Grid.Column>
                  </Grid.Row>
                </Grid>
              </div>
              }
              </div>
            </Form>
        </Container>
    )
  }
}
export default reduxForm({
  form: 'User',
  enableReinitialize: true
})(User);
