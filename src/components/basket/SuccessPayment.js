import React,{Component} from 'react'
import { Container,Segment,Grid,Button,Image} from 'semantic-ui-react'
import { Link } from 'react-router-dom'
class SuccessPayment extends Component{
  componentDidMount(){
    this.props.success(3)
  }
  render(){
    return(
      <Container>
          <Grid centered>
            <Grid.Column width={8}>
              <Segment padded>
                <Grid>
                  <Grid.Column width={8}>
                     <Image src='/img/logo.png' size='small' />
                  </Grid.Column>
                  <Grid.Column width={8}>
                    <h2>Спасибо, ваш заказ принят</h2>
                    <span>Ваш заказ №{this.props.number} принят в обработку.</span>
                    <br style={{lineHeight: 2}}/>
                    <Button as={Link} to='/cabinet/history' basic color='orange'>Перейти в личный кабинет</Button>
                  </Grid.Column>
                </Grid>
              </Segment>
              <Button as={Link} to='/' fluid={true} color='green'>Вернуться в каталог  товаров</Button>
            </Grid.Column>
          </Grid>
      </Container>
    )
  }
}
export default SuccessPayment;
