import React,{Component} from 'react'
import { Dropdown,Label,Form } from 'semantic-ui-react'
class AddressList extends Component{
  constructor(props){
    super(props)
    this.state = {
      list: []
    }
    this.changeAddress = this.changeAddress.bind(this)
  }
  componentDidMount(){
    this.props.addresslist()
  }
  componentWillReceiveProps(props){
    if(props.data.length>0){
      var arr=[]
      props.data.map(function(item,index){
        arr.push({key: index,value: item.id,text: <div>{item.city}, ул. {item.street}, дом {item.house} {item.apartment ? `,квартира ${item.apartment}` : false}</div> })
        return arr;
      })
      this.setState({list: arr})
    }

  }
  changeAddress(event,data){
    this.props.changeAddress(data.value)
  }
  render(){
    const { required,meta: { touched, error } } = this.props
    return(
      <div>
        <br/>
        <Form.Field  required={required} >
          <div>
            <Dropdown placeholder='Добавленные адресса' onChange={this.changeAddress}  fluid selection  options={this.state.list} />
            {
              touched && (error  && <Label basic color='red' pointing>{error}</Label>)
            }
          </div>
        </Form.Field>

      </div>
    )
  }
}
export default AddressList;
