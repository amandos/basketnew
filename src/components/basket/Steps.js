import React,{Component} from 'react'
import { Step,Icon} from 'semantic-ui-react'
import { Link } from 'react-router-dom'
class Steps extends Component{
  render(){
    return(
      <div>

          {
            this.props.step===3 ?
              false
            :
              <div>
                <h2>Корзина</h2>
                <Step.Group style={{width: '100%'}}>
                  <Step key='goods' active={this.props.step===0 ? true : false} >
                    <Icon color='orange' name='shopping basket' />
                    <Step.Content>
                      <Step.Title>Товары</Step.Title>
                      <Step.Description>{this.props.step===1 || this.props.step===2 ? <Link style={{fontSize: '14px'}} to='/basket/product'>Изменить товары</Link> : 'Товары в корзине'}</Step.Description>
                    </Step.Content>
                  </Step>
                  <Step key='billing' active={(this.props.step===1) ? true : false} disabled={this.props.step<1 ? true : false}>
                    <Icon color='orange' name='user' />
                    <Step.Content>
                      <Step.Title>Покупатель</Step.Title>
                      <Step.Description>{this.props.step===2 ? <Link style={{fontSize: '14px'}} to='/basket/user'>Изменить данные</Link> : ' Данные Покупателья'}</Step.Description>
                    </Step.Content>
                  </Step>
                  <Step  key='payment' active={this.props.step===2 ? true : false} disabled={this.props.step<2 ? true : false}>
                    <Icon color='orange' name='payment' />
                    <Step.Content>
                      <Step.Title>Оплата</Step.Title>
                      <Step.Description>Выберите способ оплаты</Step.Description>
                    </Step.Content>
                  </Step>
                </Step.Group>
            </div>
          }
      </div>
    )
  }
}
export default Steps;
