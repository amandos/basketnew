import React,{Component} from 'react'
import { Container,Image,Grid,Popup,List,Button} from 'semantic-ui-react'
import Steps from './Steps'
import { Link } from 'react-router-dom'
class Basket extends Component{
  constructor(props){
    super(props)
    this.logout = this.logout.bind(this)
    this.loginRoute = this.loginRoute.bind(this)
  }
  componentDidMount(){
    this.props.onUserInfo()
  }
  logout(){
    this.props.onLogout(this.props.history)
  }
  loginRoute(){
    this.props.history.push('/login')
  }
  render(){
    return(
      <div className='content_body' >
      <style>
         {`
           body > div,
           body > div > div,
           body > div > div > div.content_body {
             height: 100%;
           }
         `}
      </style>
      <div className='orangeHeader'>
        <Container >
            <Grid padded='vertically' verticalAlign='middle'>
              <Grid.Row>
                <Grid.Column>
                  <Image src='/img/logo_mini.png' size='mini' />
                </Grid.Column>
                <Grid.Column width={3} style={{textAlign: 'right'}} floated='right' verticalAlign='middle'>
                  <List floated='right'>
                    <List.Item>
                      <List.Icon onClick={this.props.userinfo.phone ? ()=>{return false} : this.loginRoute} style={{cursor: 'pointer',color: '#fff'}} name='user' size='big'   />
                      {
                        this.props.userinfo.phone ?
                            <List.Content  verticalAlign='middle'>
                              <Popup
                                style={{height:
                                'auto'}}
                                wide
                                on='click'
                                trigger={<h4 style={{float: 'left',color: '#fff',cursor: 'pointer'}}>{this.props.userinfo.name}</h4>}
                                content={<div><Button as={Link} to='/cabinet/history' basic color='green' content='Личный кабинет' fluid /><br /><Button onClick={this.logout} color='red' content='Выйти' fluid /></div>}
                                position='bottom right'
                              />
                            </List.Content>
                        :
                          false
                      }

                    </List.Item>
                  </List>
                </Grid.Column>
              </Grid.Row>
            </Grid>
        </Container>
      </div>
      <Container>
        <Steps step={this.props.step}/>
      </Container>
      <br />
      {this.props.children}
    </div>
    )
  }
}
export default Basket;
