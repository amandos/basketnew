import React,{Component} from 'react'
import { Icon,Input, Image,Table,Container,Button,Loader,Message} from 'semantic-ui-react'
var _ = require('lodash');
class Products extends Component{
  constructor(props){
    super(props)
    this.state = {
      products: [],
      total: 0,
      bonus: 0,
      error:false
    }
    this.countUp = this.countUp.bind(this)
    this.countDown= this.countDown.bind(this)
    this.inputChange = this.inputChange.bind(this,this.totalItem)
    this.totalBasket = this.totalBasket.bind(this)
    this.updateBasket = this.updateBasket.bind(this)
  }
  countUp(barcode,quantity,total){
    if((quantity<999)){
      var arr = _.filter(this.state.products, function(o) {
         if(o.barcode === barcode){
           o.quantity = parseInt(o.quantity,10)+1;
           o.totalItem = total(o.sum,o.discount,o.quantity);
         }
         return o;
       });

       this.setState({products: arr});
       this.totalBasket();
    }
  }
  countDown(barcode,quantity,total){
      if(quantity>1){
        var arr = _.filter(this.state.products, function(o) {
           if(o.barcode === barcode){
              o.quantity = parseInt(o.quantity,10)-1;
              o.totalItem = total(o.sum,o.discount,o.quantity);
           }
           return o;
        });
        this.setState({products: arr});
        this.totalBasket();
      }
  }
  inputChange(total,event,{value}){
    var arr = [];
    if(Number.isNaN(parseInt(value,10))){
       arr = _.filter(this.state.products, function(o) {
         if(parseInt(o.barcode,10) === parseInt(event.target.id,10)){
            o.quantity = 1;
            o.totalItem = total(o.sum,o.discount,o.quantity);
         }
         return o;
      });
      this.setState({products: arr});
      this.totalBasket();
    }else if((value.length<=3) && (value>0)) {
      arr = _.filter(this.state.products, function(o) {
         if(parseInt(o.barcode,10) === parseInt(event.target.id,10)){
            o.quantity = parseInt(value,10);
            o.totalItem = total(o.sum,o.discount,o.quantity);
         }
         return o;
      });
      this.setState({products: arr});
      this.totalBasket();
    }else{
      return false;
    }
  }
  totalItem(price,discount,quantity){
    quantity = parseInt(quantity,10);
    discount = parseInt(discount,10);
    price = parseInt(price,10);
    if(discount>0){
      let dissum = ((quantity*price)*discount)/100
      return parseInt((quantity*price)-dissum,10);
    }else{
      return parseInt(quantity*price,10);
    }
  }
  totalBasket(){
    var total = 0;
    this.state.products.map(function(data){
      total += data.totalItem;
      return total;
    })
    var bonus = parseInt((total*1)/100,10);
    this.setState({total: total});
    this.setState({bonus: bonus});
  }
  componentDidMount(){
    this.props.onStep();
    this.props.onBasketlist()
  }
  updateBasket(){
    if(this.state.total < 5000){
      this.setState({error: true});
    }else{
      this.setState({error: {state: false,msg: ''}});
      this.props.onBasketUpdate(this.state.products,this.props.history);
    }

  }
  componentWillReceiveProps(props){
    var arr =[];
    if(props.baksetBs.length>0){
      for(var i=0; i<=props.baksetBs.length-1; i++){
        props.baksetBs[i].totalItem = this.totalItem(props.baksetBs[i].sum,props.baksetBs[i].discount,props.baksetBs[i].quantity);
        arr.push(props.baksetBs[i]);
      }
    }
    var total = 0;
    props.baksetBs.map(function(data){
      total += data.totalItem;
      return total;
    })
    var bonus = parseInt((total*1)/100,10);
    this.setState({total: total});
    this.setState({bonus: bonus});
    this.setState({products: arr})
  }
  render(){
    return(
      <div>
        <Container style={{padding: '15px',backgroundColor: '#fff'}}>
          {
            this.props.loadingBs ?
              <Loader active/>
            :
            this.props.errorBs.status
            ?
              <Message color='red'>{this.props.errorBs.msg}</Message>
            :
            <div>
              <Table size='small' columns={5}>
                <Table.Header>
                  <Table.Row>
                    <Table.HeaderCell>Наименование</Table.HeaderCell>
                    <Table.HeaderCell></Table.HeaderCell>
                    <Table.HeaderCell singleLine>Штрих-код</Table.HeaderCell>
                    <Table.HeaderCell>Цена</Table.HeaderCell>
                    <Table.HeaderCell>Скидка</Table.HeaderCell>
                    <Table.HeaderCell>Количество</Table.HeaderCell>
                    <Table.HeaderCell>Сумма</Table.HeaderCell>
                    <Table.HeaderCell></Table.HeaderCell>
                  </Table.Row>
                </Table.Header>
                <Table.Body>
                {
                  this.state.products ? this.state.products.map(function(item,index){
                    return(
                      <Table.Row key={index}>
                        <Table.Cell>
                            <Image onError={(e)=>{e.target.src='/img/none.png'}} src={`/img/${item.barcode}.png`} size='tiny'/>
                        </Table.Cell>
                        <Table.Cell>
                          <h5>{item.name}</h5>
                        </Table.Cell>
                        <Table.Cell singleLine>{item.barcode}</Table.Cell>
                        <Table.Cell singleLine>{item.sum}&nbsp; 〒</Table.Cell>
                        <Table.Cell singleLine>{item.discount>0 ? <span>{item.discount}&nbsp; %</span> : ''}</Table.Cell>
                        <Table.Cell>
                          <Input
                            size='mini'
                            style={{width: '30%'}}
                            value={item.quantity}
                            id={item.barcode}
                            onChange={this.inputChange}
                            label={{ basic: true, content: <div>
                                <Icon onClick={()=>this.countUp(item.barcode,item.quantity,this.totalItem)}  style={{cursor: 'pointer'}} size='large' name='caret up'/><br />
                                <Icon onClick={()=>this.countDown(item.barcode,item.quantity,this.totalItem)} style={{cursor: 'pointer'}}  size='large' name='caret down' />
                              </div> }}
                            labelPosition='right'
                          />
                        </Table.Cell>
                        <Table.Cell singleLine>
                          {
                            item.discount === '0' ?
                              <h4>{item.totalItem} &nbsp; 〒</h4>
                            :
                              <h4>
                                <s style={{color: '#999'}}>
                                  { parseInt(item.quantity*item.sum,10)} &nbsp; 〒
                                </s>
                                <br />
                                {item.totalItem} &nbsp; 〒
                              </h4>
                          }
                        </Table.Cell>
                        <Table.Cell><Icon onClick={()=>this.props.onBasketDelete(item.barcode)} style={{cursor: 'pointer'}}  size='big' name='remove circle' /></Table.Cell>
                      </Table.Row>
                    )
                  },this)
                  : false
                }
                </Table.Body>
                <Table.Footer>
                  <Table.Row>
                    <Table.HeaderCell></Table.HeaderCell>
                    <Table.HeaderCell></Table.HeaderCell>
                    <Table.HeaderCell></Table.HeaderCell>
                    <Table.HeaderCell></Table.HeaderCell>
                    <Table.HeaderCell><h3>Бонус: <span style={{color: '#58a000'}}>+ {this.state.bonus}</span></h3></Table.HeaderCell>
                    <Table.HeaderCell><h3>Итого:</h3></Table.HeaderCell>
                    <Table.HeaderCell singleLine><h3>{this.state.total} &nbsp; 〒</h3></Table.HeaderCell>
                    <Table.HeaderCell></Table.HeaderCell>
                  </Table.Row>
                </Table.Footer>
              </Table>
              {
                this.state.error
                ?
                  <Message color='red'>Уважаемые покупатели информируем вас о том. что в нашем магазине минимальная сумма заказа составляет 5000 тенге. С Уважением Администрация магазина "3D market"</Message>
                :
                  false
              }
              <Button onClick={this.updateBasket} floated='right' color='orange'>Продолжить</Button>
              <br style={{lineHeight: 4}}/>
            </div>
          }
        </Container>
      </div>
    )
  }
}
export default Products
