  import React,{Component} from 'react'
  import { Container,Button,Table,Message,Loader,Dimmer} from 'semantic-ui-react'
  import OrderProducts from '../OrderProducts'
  import SuccessPayment from './SuccessPayment'
  class Payment extends Component{
    constructor(props){
      super(props)
      this.state = {
        payment_type: 0
      }
      this.changePaymentType = this.changePaymentType.bind(this)
    }
    componentDidMount(){
      this.props.onStep(2);
      this.props.onOrderInfo(this.props.match.params.orderid);
      this.props.onSuccessFalse();
    }
    changePaymentType(type){
      if(type===1 && this.props.codePaymentCard === ''){
        this.props.onPaymentcard(this.props.match.params.orderid);
      }
      this.setState({payment_type: type});
    }
    render(){
      return(
        <div>
          {
            this.props.successPayment===false ?
              <Container style={{padding: '15px',backgroundColor: '#fff'}}>
                <Dimmer inverted active={this.props.loadingPayment}>
                  <Loader />
                </Dimmer>
                {
                  this.props.loadingOrderInfo || this.props.loadingPaymentCard ?
                  <Dimmer inverted active>
                    <Loader />
                  </Dimmer>
                  :
                  this.props.errorOrderInfo.status
                  ?
                      <Message color='red'>{this.props.errorOrderInfo.msg}</Message>
                  :
                    <div>
                      <h2>Выберите способ оплаты</h2>
                      <Button onClick={()=>this.changePaymentType(1)} type='button' color={this.state.payment_type===1 ? 'orange' : 'grey'} basic size='big'>С карточки</Button>
                      <Button onClick={()=>this.changePaymentType(2)}  color={this.state.payment_type===2 ? 'orange' : 'grey'}
                        size='big'
                        content='С баланса'
                        label={{ basic: true, color: this.state.payment_type===2 ? 'orange' : 'grey', pointing: 'left', content: this.props.orderInfo.user_info ? this.props.orderInfo.user_info[0].balance : false}} />
                      <Button onClick={()=>this.changePaymentType(3)}  color={this.state.payment_type===3 ? 'orange' : 'grey'}
                        size='big'
                        content='Бонусами'
                        label={{ basic: true, color: this.state.payment_type===3 ? 'orange' : 'grey', pointing: 'left', content: this.props.orderInfo.user_info ? this.props.orderInfo.user_info[0].bonus : false }} />
                      <Button onClick={()=>this.changePaymentType(4)}  type='button' color={this.state.payment_type===4 ? 'orange' : 'grey'} basic size='big'>Наличными</Button>
                      <h3>Номер заказа {this.props.orderInfo.info ? this.props.orderInfo.info[0].id : false}</h3>
                      <div>
                          <Table basic='very' celled={false} >
                            <Table.Body>
                              <Table.Row>
                                <Table.Cell>
                                    <h4 style={{color: '#a8a8a8'}}>Пользователь</h4>
                                </Table.Cell>
                                <Table.Cell>
                                    <h4>{this.props.orderInfo.user_info ? this.props.orderInfo.user_info[0].name : false}</h4>
                                </Table.Cell>
                              </Table.Row>
                              <Table.Row>
                                <Table.Cell>
                                    <h4 style={{color: '#a8a8a8'}}>Номер телефона</h4>
                                </Table.Cell>
                                <Table.Cell>
                                    <h4>{this.props.orderInfo.user_info ? this.props.orderInfo.user_info[0].phone : false}</h4>
                                </Table.Cell>
                              </Table.Row>
                              <Table.Row>
                                <Table.Cell>
                                    <h4 style={{color: '#a8a8a8'}}>Адресс доставки:</h4>
                                </Table.Cell>
                                <Table.Cell>
                                    <h4>{this.props.orderInfo.info ? <div>{this.props.orderInfo.info[0].city}, ул. {this.props.orderInfo.info[0].street}, дом {this.props.orderInfo.info[0].house} {this.props.orderInfo.info[0].apartment ? `,квартира ${this.props.orderInfo.info[0].apartment}` : false}</div> : false}</h4>
                                </Table.Cell>
                              </Table.Row>
                              <Table.Row>
                                <Table.Cell>
                                    <h4 style={{color: '#a8a8a8'}}>Время доставки:</h4>
                                </Table.Cell>
                                <Table.Cell>
                                    <h4>{this.props.orderInfo.info ? this.props.orderInfo.info[0].delivery_time : false}</h4>
                                </Table.Cell>
                              </Table.Row>
                            </Table.Body>
                          </Table>
                      </div>
                      {
                        this.props.orderInfo.products ?
                          <OrderProducts products={this.props.orderInfo.products} />
                        : false

                      }
                      {
                        this.props.errorPayment.status ?
                          <Message color='red'>{this.props.errorPayment.msg}</Message>
                        :
                          false
                      }
                      {
                        this.state.payment_type === 1 ?
                          <form method="POST" action="https://testpay.kkb.kz/jsp/process/logon.jsp">
                            <input type="hidden" name="Signed_Order_B64" value={this.props.codePaymentCard}/>
                            <input type="hidden" name="Language" value="rus"/>
                            <input type="hidden" name="BackLink" value="http://192.168.1.57/cabinet/history"/>
                            <input type="hidden" name="PostLink" value="http://192.168.1.57/apicabinet/pay.php"/>
                            <input type="hidden" name="FailureBackLink" value="http://192.168.1.57/"/>
                            <input type="hidden" name="template" value="default.xsl"/>
                            <Button  type="submit" floated='right' color='orange'>Оплатить</Button>
                            <br style={{lineHeight: 4}}/>
                        </form>
                        :
                        <div>
                          <Button disabled={this.props.loadingPayment} onClick={()=>this.props.onPayment(this.props.match.params.orderid,this.state.payment_type)} floated='right' color='orange'>Оплатить</Button>
                          <br style={{lineHeight: 4}}/>
                        </div>
                      }

                    </div>
                }

              </Container>
            :
              <SuccessPayment number={this.props.match.params.orderid} success={this.props.onStep}/>
          }
      </div>
      )
    }
  }
  export default Payment;
