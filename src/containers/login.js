import { connect } from 'react-redux'
import { loginAction} from '../actions/login'
import Login from '../components/Login'
import { withRouter } from 'react-router'
var _ = require('lodash');
const mapStateToProps = (state) => {
  return {
    loading: state.login.loading,
    errorserver: state.login.errorserver,
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    OnLogin: (values,history)=>{
      dispatch(loginAction(values,history));
    }

  }
}
export const LoginContainer = withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
  )(Login))
