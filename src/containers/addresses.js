import { connect } from 'react-redux'
import { addressList} from '../actions/addressList'
import { addAddress} from '../actions/addaddress'
import { delAddress} from '../actions/deladdress'
import Addresses from '../components/Addresses'
import { withRouter } from 'react-router'
const mapStateToProps = (state) => {
  return {
    loadingAd: state.addresslist.loading,
    addresslist: state.addresslist.data,
    errorserverAd: state.addresslist.errorserver,
    loadingAds: state.addaddress.loading,
    errorserverAds: state.addaddress.errorserver,
    loadingAdsDl: state.deladdress.loading,
    errorserverAdsDl: state.deladdress.errorserver,

  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    onUserAddress: ()=>{
      dispatch(addressList())
    },
    onAddressAdd: (values)=>{
      dispatch(addAddress(values))
    },
    onDelAddress: (value)=>{
      dispatch(delAddress(value))
    }
  }
}
export const AddressesCont = withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(Addresses))
