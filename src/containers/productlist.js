import { connect } from 'react-redux'
import { productlist} from '../actions/productlist'
import { basketAdd} from '../actions/basketAdd'
import Productlist from '../components/Productlist'
import { withRouter } from 'react-router'
var _ = require('lodash');
const mapStateToProps = (state) => {
  return {
    loadingPr: state.productlist.loading,
    products:state.productlist.data,
    errorPr: state.productlist.errorserver,
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    onProductlist: ()=>{
      dispatch(productlist());
    },
    onBasketadd: (barcode)=>{
      dispatch(basketAdd(barcode));
    }

  }
}
export const ProductlistContainer = withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
  )(Productlist))
