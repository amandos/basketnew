import { connect } from 'react-redux'
import { registration} from '../actions/registration'
import Registration from '../components/Registration'
import { withRouter } from 'react-router'
var _ = require('lodash');
const mapStateToProps = (state) => {
  return {
    loadingRg: state.registration.loading,
    errorRg: state.registration.errorserver,
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    onRegistration: (values,history)=>{
      dispatch(registration(values,history));
    }

  }
}
export const RgtnCont = withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
  )(Registration))
