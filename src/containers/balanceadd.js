import { connect } from 'react-redux'
import { balanceadd} from '../actions/balanceadd'
import Addbalance from '../components/Addbalance'
import { withRouter } from 'react-router'
const mapStateToProps = (state) => {
  return {
    loadingBA: state.balanceadd.loading,
    errorBA: state.balanceadd.errorserver,
    codeBA: state.balanceadd.code,

  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    onBalanceAdd: (sum)=>{
      dispatch(balanceadd(sum))
    }
  }
}
export const AddbalanceCont = withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(Addbalance))
