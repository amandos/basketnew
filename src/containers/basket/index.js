import { connect } from 'react-redux'
import Basket from '../../components/basket'
import { withRouter } from 'react-router'
import { InfoAction} from '../../actions/userinfo'
import { logout} from '../../actions/login'
const mapStateToProps = (state) => {
  return {
    step: state.step.step,
    userinfo: state.userinfo.userinfo,
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    onUserInfo: ()=>{
      dispatch(InfoAction());
    },
    onLogout: (history)=>{
      dispatch(logout(history));
    }
  }
}
export const BasketCont = withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
  )(Basket))
