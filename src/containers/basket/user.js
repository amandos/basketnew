import { connect } from 'react-redux'
import User from '../../components/basket/User'
import { orderInsert} from '../../actions/orderInsert'
import { InfoAction} from '../../actions/userinfo'
import { addressList} from '../../actions/addressList'
import { step} from '../../actions/step'
import { withRouter } from 'react-router'
import {reactLocalStorage} from 'reactjs-localstorage';
var _ = require('lodash');
var base64 = require('base-64');
const mapStateToProps = (state) => {
  return {
    loading: state.orderInsert.loading,
    errorserver: state.orderInsert.errorserver,
    userinfo: state.userinfo.userinfo,
    addresslist: state.addresslist.data,
    initialValues:{
      street:reactLocalStorage.get('street') ? reactLocalStorage.get('street') : '',
      house:reactLocalStorage.get('house') ? reactLocalStorage.get('house') : '',
      apartment:reactLocalStorage.get('apartment') ? reactLocalStorage.get('apartment') : '',
    }

  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    onStep: ()=>{
      dispatch(step(1));
    },
    onOrderInsert:  (values,history)=>{
      dispatch(orderInsert(values,history))
    },
    onUserCheck: ()=>{
      dispatch(InfoAction())
    },
    onBasketProducts: ()=>{
      let basket = JSON.parse(base64.decode(reactLocalStorage.get('productsBasket')));
      return basket;
    },
    onUserAddress: ()=>{
      dispatch(addressList())
    }
  }
}
export const BtUserCont = withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
  )(User))
