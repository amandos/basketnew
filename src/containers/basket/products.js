import { connect } from 'react-redux'
import { basketList} from '../../actions/basket'
import { step} from '../../actions/step'
import Products from '../../components/basket/Product'
import { withRouter } from 'react-router'
import {reactLocalStorage} from 'reactjs-localstorage';
var _ = require('lodash');
var base64 = require('base-64');
const mapStateToProps = (state) => {
  return {
    baksetBs: state.basket.products,
    loadingBs: state.basket.loading,
    errorBs:state.basket.errorserver,
    countBs: state.basket.count
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    onStep: ()=>{
      dispatch(step(0));
    },
    onBasketlist: ()=>{
      dispatch(basketList());
    },
    onBasketDelete: (barcode)=>{
      let basket = JSON.parse(base64.decode(reactLocalStorage.get('productsBasket')));
      var evens = _.remove(basket, function(n) {
        return n.barcode !== barcode;
      });
      basket = base64.encode(JSON.stringify(evens,true));
      reactLocalStorage.set('productsBasket',basket);
      dispatch(basketList());
    },
    onBasketUpdate: (values,history)=>{
      var arr=[];
      values.map(function(item){
        arr.push({barcode: item.barcode,quantity: item.quantity})
        return arr;
      })
      var basket = base64.encode(JSON.stringify(arr,true));
      reactLocalStorage.set('productsBasket',basket);
      history.push('/basket/user')
    }
  }
}
export const BtPrdtsCont = withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
  )(Products))
