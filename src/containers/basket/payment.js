import { connect } from 'react-redux'
import Payment from '../../components/basket/Payment'
import { orderInfo} from '../../actions/orderInfo'
import { payment} from '../../actions/payment'
import { paymentcard} from '../../actions/paymentcard'
import {successFalse} from '../../actions/payment'
import { step} from '../../actions/step'
import { withRouter } from 'react-router'

const mapStateToProps = (state) => {
  return {
    loadingOrderInfo: state.orderinfo.loading,
    errorOrderInfo: state.orderinfo.errorserver,
    orderInfo: state.orderinfo.data,
    loadingPayment: state.payment.loading,
    errorPayment: state.payment.errorserver,
    successPayment: state.payment.success,
    loadingPaymentCard: state.paymentcard.loading,
    codePaymentCard: state.paymentcard.code
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    onStep: (status)=>{
      dispatch(step(status));
    },
    onOrderInfo: (id)=>{
      dispatch(orderInfo(id))
    },
    onPayment: (id,type)=>{
      dispatch(payment(id,type))
    },
    onSuccessFalse: ()=>{
      dispatch(successFalse())
    },
    onPaymentcard: (id)=>{
      dispatch(paymentcard(id,1));
    }
  }
}
export const BtPaymentCont = withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
  )(Payment))
