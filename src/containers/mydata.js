import { connect } from 'react-redux'
import { InfoAction} from '../actions/userinfo'
import { editUser} from '../actions/edituser'
import { editpass} from '../actions/editpass'
import Mydata from '../components/Mydata'
import { withRouter } from 'react-router'
const mapStateToProps = (state) => {
  return {
    userinfo: state.userinfo.userinfo,
    loadingEd: state.edituser.loading,
    errorserverEd:state.edituser.errorserver,
    loadingEp: state.editpass.loading,
    errorserverEp:state.editpass.errorserver,
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    onUserInfo: ()=>{
      dispatch(InfoAction());
    },
    onEditMydata: (values)=>{
      dispatch(editUser(values));
    },
    onEditPass: (values)=>{
      dispatch(editpass(values));
    },

  }
}
export const MydataCont = withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
  )(Mydata))
