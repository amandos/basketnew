import { connect } from 'react-redux'
import { orderList} from '../actions/orders'
import { orderInfo} from '../actions/orderInfo'
import History from '../components/History'
import { withRouter } from 'react-router'
import {reactLocalStorage} from 'reactjs-localstorage';
var _ = require('lodash');
var base64 = require('base-64');
const mapStateToProps = (state) => {
  return {
    loadingOrderlist:state.orderlist.loading,
    errorOrderlist: state.orderlist.errorserver,
    orderlist: state.orderlist.data,
    orderInfo: state.orderinfo.data,
    loadingOrderinfo:state.orderinfo.loading,
    errorOrderinfo: state.orderinfo.errorserver,
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    OnOrderlist: ()=>{
      dispatch(orderList());
    },
    OnOrderinfo: (order_id)=>{
      dispatch(orderInfo(order_id));
    },
    OnOrderReload: (product,history)=>{
      var arr=[];
      product.map(function(item){
        arr.push({barcode: item.barcode,quantity: item.quantity})
        return arr;
      })
      var products = base64.encode(JSON.stringify(arr,true));
      reactLocalStorage.set('productsBasket',products);
      history.push('/basket/product')
    }
  }
}
export const HistoryCont = withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
  )(History))
