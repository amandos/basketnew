import { connect } from 'react-redux'
import { InfoAction} from '../actions/userinfo'
import { logout} from '../actions/login'
import Layout from '../components/Layout'
import { withRouter } from 'react-router'
const mapStateToProps = (state) => {
  return {
    userinfo: state.userinfo.userinfo,
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    onUserInfo: ()=>{
      dispatch(InfoAction());
    },
    onLogout: (history)=>{
      dispatch(logout(history));
    }

  }
}
export const LayoutCont = withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
  )(Layout))
